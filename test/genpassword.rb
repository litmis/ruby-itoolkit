require 'xmlservice'
require 'active_record'
require 'ibm_db'
require 'pathname'

puts "Input: "
ARGV.each do|a|
  puts "  Argument: #{a}"
end

puts "Output: "
if (ARGV.count < 3)
  puts "Syntax:"
  puts "  ruby genpassword.rb db user password [key]"
  puts "    db       - *LOCAL (see WRKRDBDIRE)"
  puts "    user     - user profile id"
  puts "    password - user profile password"
  puts "  Optional:"
  puts "    key      - pass key (generate if missing)"
  puts "             - /path/key.yml                 "
  puts "             - YIPS4321AAAAAAAAAAAAAAA132424245 (actual key)"
  exit(-1)
end

$db = ARGV[0]
$user = ARGV[1]
$pass = ARGV[2]
# $db = "LP0364D"
# $user = "DB2"
# $pass = "NICE2DB2"
ActiveXMLService::Base.establish_connection(
    :adapter   => 'ibm_db',
    :database  => $db,
    :username  => $user,
    :password  => $pass,
    :connection => "ActiveRecord",
    :install    => "XMLSERVICE",
    :ctl        => "*sbmjob",
    :ipc        => "/tmp/ruby042",
    :size       => 15000000,
    :head       => "<?xml version='1.0'?>"
)
$isnew = false
if (ARGV.count < 4) 
  $key = ActiveXMLService::Base.generate_key()
  $isnew = true
else
  $key = ARGV[3]
  if $key.include? ".yml"
    yaml_file = $key
    rfile = Pathname.new(yaml_file)
    if rfile
      f = open(rfile.to_s)
      doc = YAML::load_stream( f )
      doc.each do |key, value|
        key.each do |key0, value0|
          $key = value0
        end
      end
    end
  end
end
penc = ActiveXMLService::Base.generate_password($pass,$key)
puts
puts "config/database.yml"
puts "xmlservice: &xmlservice"
puts "  connection: ActiveRecord"
puts "  install: XMLSERVICE"
puts "  ctl: *sbmjob"
puts "  ipc: /tmp/ruby042"
puts "  size: 15000000"
puts "  head: \"<?xml version='1.0'?>\""
puts "development:"
puts "  adapter: ibm_db"
puts "  database: #{$db}"
puts "  username: #{$user}"
puts "  pwd_yaml: /path/password.yml"
puts "  -- or --"
puts "  pwd_enc: #{penc}"
puts "  key_yaml: /path/key.yml"
puts "  <<: *xmlservice"
puts "production:"
puts "  adapter: ibm_db"
puts "  database: #{$db}"
puts "  username: #{$user}"
puts "  pwd_yaml: /path/password.yml"
puts "  <<: *xmlservice"
puts "test:"
puts "  adapter: ibm_db"
puts "  database: #{$db}"
puts "  username: #{$user}"
puts "  pwd_yaml: /path/password.yml"
puts "  <<: *xmlservice"
puts
puts "/path/password.yml"
puts "#{$user}:"
puts "  pwd_enc: #{penc}"
puts "  key_yaml: /path/key.yml"
puts
if $isnew
  puts "Warning: "
  puts "  pwd_key is new (generated now)"
  puts "  re-generate any exsisting"
  puts "  password.yml encrypted passwords"
  puts "  with a new key.yml (below)"
end
puts
puts "/path/key.yml"
puts "pwd_key: #{$key}"


