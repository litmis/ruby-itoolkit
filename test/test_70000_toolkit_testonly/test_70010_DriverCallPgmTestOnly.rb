# File: test_70010_DriverCallPgmZZTEST.rb
ENV['TEST_ENV'] = "resttest"
require "../test_authorization/auth"
require "../test_data/rowcol"

class Test_70010_DriverCallPgmTestOnly < RowColUnitTest
  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------
  def test_0040_pgm_zztest_index_output
    # call IBM i ZZTEST (see file ../test_data/zztest)
    in_parm1 = {'inchara'=>'a','incharb'=>'b','indec1'=>11.1111,'indec2'=>222.22}
    in_inds1 = {'dschara'=>'x','dscharb'=>'y','dsdec1'=>66.6666,'dsdec2'=>77777.77}
    zztest = XMLService::I_PGM.new("ZZTEST", $toolkit_test_lib) << 
      XMLService::I_a.new('inchara',1,  in_parm1['inchara']) << 
      XMLService::I_a.new('incharb',1,  in_parm1['incharb']) << 
      XMLService::I_p.new('indec1',7,4, in_parm1['indec1']) << 
      XMLService::I_p.new('indec2',12,2,in_parm1['indec2']) << 
      XMLService::I_DS.new("inds1",5,
      [ 
       XMLService::I_a.new('ds1chara',1,  in_inds1['dschara']),
       XMLService::I_a.new('ds1charb',1,  in_inds1['dscharb']),
       XMLService::I_p.new('ds1dec1',7,4, in_inds1['dsdec1']),
       XMLService::I_p.new('ds1dec2',12,2,in_inds1['dsdec2']),
       XMLService::I_DS.new("inds2",2,
       [ 
        XMLService::I_a.new('ds2chara',1,  in_inds1['dschara']),
        XMLService::I_a.new('ds2charb',1,  in_inds1['dscharb']),
        XMLService::I_p.new('ds2dec1',7,4, in_inds1['dsdec1']),
        XMLService::I_p.new('ds2dec2',12,2,in_inds1['dsdec2']),
        XMLService::I_DS.new("inds3",3,
        [ 
         XMLService::I_a.new('ds3chara',1,  in_inds1['dschara']),
         XMLService::I_a.new('ds3charb',1,  in_inds1['dscharb']),
         XMLService::I_p.new('ds3dec1',7,4, in_inds1['dsdec1']),
         XMLService::I_p.new('ds3dec2',12,2,in_inds1['dsdec2'])
        ]),
        XMLService::I_a.new('ds5chara',1,  in_inds1['dschara']),
        XMLService::I_a.new('ds5charb',1,  in_inds1['dscharb']),
        XMLService::I_p.new('ds5dec1',7,4, in_inds1['dsdec1']),
        XMLService::I_p.new('ds5dec2',12,2,in_inds1['dsdec2'])
       ]),
       XMLService::I_a.new('ds4chara',1,  in_inds1['dschara']),
       XMLService::I_a.new('ds4charb',1,  in_inds1['dscharb']),
       XMLService::I_p.new('ds4dec1',7,4, in_inds1['dsdec1']),
       XMLService::I_p.new('ds4dec2',12,2,in_inds1['dsdec2'])
      ])
    # call IBM i ZZTEST
    zztest.xmlservice
    # inspect
    puts zztest.dump_inspect
	  # xmlservice error occurred?
    rc = zztest.xmlservice_error
    if rc
      puts zztest.dump_all()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # output
    puts "#{zztest.name}:"
    puts " inchara...#{zztest.response.inchara}"
    puts " incharb...#{zztest.response.incharb}"
    puts " indec1....#{zztest.response.indec1}"
    puts " indec2....#{zztest.response.indec2}"
    for i in 0..zztest.response.inds1.count-1
      inds1 = zztest.response.inds1[i]
      puts " inds1[#{i}]:"
      puts "  ds1chara...#{inds1.ds1chara}"
      puts "  ds1charb...#{inds1.ds1charb}"
      puts "  ds1dec1....#{inds1.ds1dec1}"
      puts "  ds1dec2....#{inds1.ds1dec2}"
      for j in 0..inds1.inds2.count-1
        inds2 = inds1.inds2[j]
        puts " inds2[#{j}]:"
        puts "  ds2chara...#{inds2.ds2chara}"
        puts "  ds2charb...#{inds2.ds2charb}"
        puts "  ds2dec1....#{inds2.ds2dec1}"
        puts "  ds2dec2....#{inds2.ds2dec2}"
        for k in 0..inds2.inds3.count-1
          inds3 = inds2.inds3[j]
          puts " inds3[#{k}]:"
          puts "  ds3chara...#{inds3.ds3chara}"
          puts "  ds3charb...#{inds3.ds3charb}"
          puts "  ds3dec1....#{inds3.ds3dec1}"
          puts "  ds3dec2....#{inds3.ds3dec2}"
        end
        puts " inds2[#{j}]:"
        puts "  ds5chara...#{inds2.ds5chara}"
        puts "  ds5charb...#{inds2.ds5charb}"
        puts "  ds5dec1....#{inds2.ds5dec1}"
        puts "  ds5dec2....#{inds2.ds5dec2}"
      end
      puts " inds1[#{i}]:"
      puts "  ds4chara...#{inds1.ds4chara}"
      puts "  ds4charb...#{inds1.ds4charb}"
      puts "  ds4dec1....#{inds1.ds4dec1}"
      puts "  ds4dec2....#{inds1.ds4dec2}"
    end
    # verify
    self.match_value(__method__,__LINE__,'inchara' ,in_parm1['inchara']    ,zztest.response.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,in_parm1['incharb']    ,zztest.response.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,in_parm1['indec1']     ,zztest.response.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,in_parm1['indec2']     ,zztest.response.indec2)
    for i in 0..zztest.response.inds1.count - 1
      inds1 = zztest.response.inds1[i]
      self.match_value(__method__,__LINE__,'ds1chara' ,in_inds1['dschara']  ,inds1.ds1chara)
      self.match_value(__method__,__LINE__,'ds1charb' ,in_inds1['dscharb']  ,inds1.ds1charb)
      self.match_value(__method__,__LINE__,'ds1dec1'  ,in_inds1['dsdec1']   ,inds1.ds1dec1)
      self.match_value(__method__,__LINE__,'ds1dec2'  ,in_inds1['dsdec2']   ,inds1.ds1dec2)
      for j in 0..inds1.inds2.count - 1
        inds2 = inds1.inds2[j]
        self.match_value(__method__,__LINE__,'ds2chara' ,in_inds1['dschara']  ,inds2.ds2chara)
        self.match_value(__method__,__LINE__,'ds2charb' ,in_inds1['dscharb']  ,inds2.ds2charb)
        self.match_value(__method__,__LINE__,'ds2dec1'  ,in_inds1['dsdec1']   ,inds2.ds2dec1)
        self.match_value(__method__,__LINE__,'ds2dec2'  ,in_inds1['dsdec2']   ,inds2.ds2dec2)
        for k in 0..inds2.inds3.count - 1
          inds3 = inds2.inds3[k]
          self.match_value(__method__,__LINE__,'ds2chara' ,in_inds1['dschara']  ,inds3.ds3chara)
          self.match_value(__method__,__LINE__,'ds2charb' ,in_inds1['dscharb']  ,inds3.ds3charb)
          self.match_value(__method__,__LINE__,'ds2dec1'  ,in_inds1['dsdec1']   ,inds3.ds3dec1)
          self.match_value(__method__,__LINE__,'ds2dec2'  ,in_inds1['dsdec2']   ,inds3.ds3dec2)
        end
        self.match_value(__method__,__LINE__,'ds5chara' ,in_inds1['dschara']  ,inds2.ds5chara)
        self.match_value(__method__,__LINE__,'ds5charb' ,in_inds1['dscharb']  ,inds2.ds5charb)
        self.match_value(__method__,__LINE__,'ds5dec1'  ,in_inds1['dsdec1']   ,inds2.ds5dec1)
        self.match_value(__method__,__LINE__,'ds5dec2'  ,in_inds1['dsdec2']   ,inds2.ds5dec2)
      end
      self.match_value(__method__,__LINE__,'ds4chara' ,in_inds1['dschara']  ,inds1.ds4chara)
      self.match_value(__method__,__LINE__,'ds4charb' ,in_inds1['dscharb']  ,inds1.ds4charb)
      self.match_value(__method__,__LINE__,'ds4dec1'  ,in_inds1['dsdec1']   ,inds1.ds4dec1)
      self.match_value(__method__,__LINE__,'ds4dec2'  ,in_inds1['dsdec2']   ,inds1.ds4dec2)
    end
  end

  def test_0050_pgm_zztest_each_output
    # call IBM i ZZTEST (see file ../test_data/zztest)
    in_parm1 = {'inchara'=>'a','incharb'=>'b','indec1'=>11.1111,'indec2'=>222.22}
    in_inds1 = {'dschara'=>'x','dscharb'=>'y','dsdec1'=>66.6666,'dsdec2'=>77777.77}
    zztest = XMLService::I_PGM.new("ZZTEST", $toolkit_test_lib) << 
      XMLService::I_a.new('inchara',1,  in_parm1['inchara']) << 
      XMLService::I_a.new('incharb',1,  in_parm1['incharb']) << 
      XMLService::I_p.new('indec1',7,4, in_parm1['indec1']) << 
      XMLService::I_p.new('indec2',12,2,in_parm1['indec2']) << 
      XMLService::I_DS.new("inds1",5,
      [ 
       XMLService::I_a.new('ds1chara',1,  in_inds1['dschara']),
       XMLService::I_a.new('ds1charb',1,  in_inds1['dscharb']),
       XMLService::I_p.new('ds1dec1',7,4, in_inds1['dsdec1']),
       XMLService::I_p.new('ds1dec2',12,2,in_inds1['dsdec2']),
       XMLService::I_DS.new("inds2",2,
       [ 
        XMLService::I_a.new('ds2chara',1,  in_inds1['dschara']),
        XMLService::I_a.new('ds2charb',1,  in_inds1['dscharb']),
        XMLService::I_p.new('ds2dec1',7,4, in_inds1['dsdec1']),
        XMLService::I_p.new('ds2dec2',12,2,in_inds1['dsdec2']),
        XMLService::I_DS.new("inds3",3,
        [ 
         XMLService::I_a.new('ds3chara',1,  in_inds1['dschara']),
         XMLService::I_a.new('ds3charb',1,  in_inds1['dscharb']),
         XMLService::I_p.new('ds3dec1',7,4, in_inds1['dsdec1']),
         XMLService::I_p.new('ds3dec2',12,2,in_inds1['dsdec2'])
        ]),
        XMLService::I_a.new('ds5chara',1,  in_inds1['dschara']),
        XMLService::I_a.new('ds5charb',1,  in_inds1['dscharb']),
        XMLService::I_p.new('ds5dec1',7,4, in_inds1['dsdec1']),
        XMLService::I_p.new('ds5dec2',12,2,in_inds1['dsdec2'])
       ]),
       XMLService::I_a.new('ds4chara',1,  in_inds1['dschara']),
       XMLService::I_a.new('ds4charb',1,  in_inds1['dscharb']),
       XMLService::I_p.new('ds4dec1',7,4, in_inds1['dsdec1']),
       XMLService::I_p.new('ds4dec2',12,2,in_inds1['dsdec2'])
      ])
    # call IBM i ZZTEST
    zztest.xmlservice
    # inspect
    puts zztest.dump_inspect
    # xmlservice error occurred?
    rc = zztest.xmlservice_error
    if rc
      puts zztest.dump_all()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # output
    puts "#{zztest.name}:"
    puts " inchara...#{zztest.response.inchara}"
    puts " incharb...#{zztest.response.incharb}"
    puts " indec1....#{zztest.response.indec1}"
    puts " indec2....#{zztest.response.indec2}"
    inds1s = zztest.response.inds1
    inds1s.each do |inds1|
      puts " inds1[]:"
      puts "  ds1chara...#{inds1.ds1chara}"
      puts "  ds1charb...#{inds1.ds1charb}"
      puts "  ds1dec1....#{inds1.ds1dec1}"
      puts "  ds1dec2....#{inds1.ds1dec2}"
      inds2s = inds1.inds2
      inds2s.each do |inds2|
        puts " inds2[]:"
        puts "  ds2chara...#{inds2.ds2chara}"
        puts "  ds2charb...#{inds2.ds2charb}"
        puts "  ds2dec1....#{inds2.ds2dec1}"
        puts "  ds2dec2....#{inds2.ds2dec2}"
        inds3s = inds2.inds3
        inds3s.each do |inds3|
          puts " inds3[]:"
          puts "  ds3chara...#{inds3.ds3chara}"
          puts "  ds3charb...#{inds3.ds3charb}"
          puts "  ds3dec1....#{inds3.ds3dec1}"
          puts "  ds3dec2....#{inds3.ds3dec2}"
        end
        puts " inds2[]:"
        puts "  ds5chara...#{inds2.ds5chara}"
        puts "  ds5charb...#{inds2.ds5charb}"
        puts "  ds5dec1....#{inds2.ds5dec1}"
        puts "  ds5dec2....#{inds2.ds5dec2}"
      end
      puts " inds1[]:"
      puts "  ds4chara...#{inds1.ds4chara}"
      puts "  ds4charb...#{inds1.ds4charb}"
      puts "  ds4dec1....#{inds1.ds4dec1}"
      puts "  ds4dec2....#{inds1.ds4dec2}"
    end
    # verify
    self.match_value(__method__,__LINE__,'inchara' ,in_parm1['inchara']    ,zztest.response.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,in_parm1['incharb']    ,zztest.response.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,in_parm1['indec1']     ,zztest.response.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,in_parm1['indec2']     ,zztest.response.indec2)
    inds1s = zztest.response.inds1
    inds1s.each do |inds1|
      self.match_value(__method__,__LINE__,'ds1chara' ,in_inds1['dschara']  ,inds1.ds1chara)
      self.match_value(__method__,__LINE__,'ds1charb' ,in_inds1['dscharb']  ,inds1.ds1charb)
      self.match_value(__method__,__LINE__,'ds1dec1'  ,in_inds1['dsdec1']   ,inds1.ds1dec1)
      self.match_value(__method__,__LINE__,'ds1dec2'  ,in_inds1['dsdec2']   ,inds1.ds1dec2)
      inds2s = inds1.inds2
      inds2s.each do |inds2|
        self.match_value(__method__,__LINE__,'ds2chara' ,in_inds1['dschara']  ,inds2.ds2chara)
        self.match_value(__method__,__LINE__,'ds2charb' ,in_inds1['dscharb']  ,inds2.ds2charb)
        self.match_value(__method__,__LINE__,'ds2dec1'  ,in_inds1['dsdec1']   ,inds2.ds2dec1)
        self.match_value(__method__,__LINE__,'ds2dec2'  ,in_inds1['dsdec2']   ,inds2.ds2dec2)
        inds3s = inds2.inds3
        inds3s.each do |inds3|
          self.match_value(__method__,__LINE__,'ds2chara' ,in_inds1['dschara']  ,inds3.ds3chara)
          self.match_value(__method__,__LINE__,'ds2charb' ,in_inds1['dscharb']  ,inds3.ds3charb)
          self.match_value(__method__,__LINE__,'ds2dec1'  ,in_inds1['dsdec1']   ,inds3.ds3dec1)
          self.match_value(__method__,__LINE__,'ds2dec2'  ,in_inds1['dsdec2']   ,inds3.ds3dec2)
        end
        self.match_value(__method__,__LINE__,'ds5chara' ,in_inds1['dschara']  ,inds2.ds5chara)
        self.match_value(__method__,__LINE__,'ds5charb' ,in_inds1['dscharb']  ,inds2.ds5charb)
        self.match_value(__method__,__LINE__,'ds5dec1'  ,in_inds1['dsdec1']   ,inds2.ds5dec1)
        self.match_value(__method__,__LINE__,'ds5dec2'  ,in_inds1['dsdec2']   ,inds2.ds5dec2)
      end
      self.match_value(__method__,__LINE__,'ds4chara' ,in_inds1['dschara']  ,inds1.ds4chara)
      self.match_value(__method__,__LINE__,'ds4charb' ,in_inds1['dscharb']  ,inds1.ds4charb)
      self.match_value(__method__,__LINE__,'ds4dec1'  ,in_inds1['dsdec1']   ,inds1.ds4dec1)
      self.match_value(__method__,__LINE__,'ds4dec2'  ,in_inds1['dsdec2']   ,inds1.ds4dec2)
    end
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end

