class I_a
  # value get/set not exist until created
  # accessor inchara created dynamically
  attr_accessor :inchara
  def initialize(v)
    @inchara = v
  end
end

# param anchor for call input, repond
class I_Parameter
  attr_accessor :elem
  def initialize(elem)
    @elem = elem
  end
  # methods get/set do not exist until created
  # forwarding inchara methods created dynamically
  def inchara
    @elem.inchara
  end
  def inchara=(value)
    @elem.inchara=value
  end
end

# class will allow area to dynamically create
# entire object stack without prefixes
# (assuming 'clever naming' user of course)
class I_Data
  # nothing in this class until dynamically created 
  attr_accessor :PARM0
  def initialize(elem)
    @PARM0 = elem
  end
  # forwarding inchara methods created dynamically
  def inchara
    @PARM0.inchara
  end
  def inchara=(value)
    @PARM0.inchara=value
  end
end

# main/top class
# assuming 'clever naming' user of course
# :input = I_Data.new
# :respond = I_Data.new
# (:returndata = I_Data.new)
class I_Pgm
  attr_accessor :input, :respond
  def initialize
    @input = I_Data.new ( I_Parameter.new(I_a.new('a')) )
  end
end

# customer script usage
zzcall = I_Pgm.new
puts zzcall.input.PARM0.inchara

zzcall.input.PARM0.inchara = 'b'
puts zzcall.input.PARM0.inchara

zzcall.input.inchara = 'c'
puts zzcall.input.inchara


