# File: test_60910_DriverCallPgmzzmiss.rb
require "../test_authorization/auth"
require "../test_data/rowcol"

class Test_60910_DriverCallPgmZZMISS < RowColUnitTest
  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------
  def test_0030_pgm_zzmiss_error
    # call missing PGM
    # 'error' => 'on' -- slow large joblog (development)
    # 'error' => 'off' -- slow subset joblog (development)
    # 'error' => 'fast' -- high speed errors (production)
    options = {'error'=>'off'} 
    zzmiss = XMLService::I_PGM.new("ZZMISS",$toolkit_test_lib,options)
    # call IBM i zzmiss
    zzmiss.xmlservice

    # xmlservice error occurred?
    puts "\n+++++++++ dump_error ++++++++++++++++"
    rc = zzmiss.xmlservice_error
    if rc
      puts zzmiss.dump_error
    end


    # puts zzmiss.inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts zzmiss.dump_inspect

    self.match_value(__method__,__LINE__,'error' ,true,rc)
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end

