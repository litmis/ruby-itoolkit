require 'xmlservice'
require 'thread'

ActiveXMLService::Base.establish_connection(
  :test_yaml => "../test_authorization/xmlservice.yml",
  :test_env  => "sqlpublic"
)

def foo1(n)
  puts "run #{n}\n"
  cmd = XMLService::I_SH.new("system -i 'WRKSYSVAL OUTPUT(*PRINT)'")
  cmd.xmlservice
  output = cmd.out_xml
  if output.include? "User part of the library list"
    puts "good #{n}\n"
  else
    puts output
    puts "bad #{n}\n"
  end
end

def foo2(n)
  puts "run #{n}\n"
  cmd = XMLService::I_CMD.new("RTVJOBA USRLIBL(?) SYSLIBL(?) CCSID(?N) OUTQ(?)")
  cmd.xmlservice
  output = cmd.out_xml
  if output.include? "desc='OUTQ'"
    puts "good #{n}\n"
  else
    puts output
    puts "bad #{n}\n"
  end
end


def foo3(n)
  puts "run #{n}\n"
  mybin = 0xA1A2A3A4A5A6A7A8A9AAA1A2A3A4A5A6A7A8A9AA
  zzbinary = XMLService::I_SRVPGM.new("ZZSRV","ZZBINARY",$toolkit_test_lib)
  zzbinary.inputParameter("parm","io",XMLService::I_Binary.new('myIn',20,mybin))
  zzbinary.setReturn("binary", XMLService::I_Binary.new('myOut',20,0x00))
  # call IBM i
  zzbinary.xmlservice
  if mybin.to_s(16).upcase == zzbinary.xml_binary.xml_myOut.to_s
    puts "good #{n}\n"
  else
    puts output
    puts "bad #{n}\n"
  end
end


begin
  threads = []
  [1,2,3,4,5,6,7,8,9,10,11,12].each do |i|
    threads << Thread.new do
      puts "loop #{i}\n"
      foo1(i)
    end
  end
  [101,102,103,104,105,106,107,108,109,1010,1011,1012].each do |i|
    threads << Thread.new do
      puts "loop #{i}\n"
      foo2(i)
    end
  end
  [201,202,203,204,205,206,207,208,209,2010,2011,2012].each do |i|
    threads << Thread.new do
      puts "loop #{i}\n"
      foo2(i)
    end
  end
  threads.each(&:join)
rescue Exception => e
  puts "EXCEPTION: #{e.inspect}"
  puts "MESSAGE: #{e.message}"
end

