# File: test_60310_DriverCMD.rb
require "../test_authorization/auth"
require "../test_data/rowcol"

class Test_60310_DriverCMD < RowColUnitTest
  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------
  def test_0020_cmd_CHGLIBL
    # ***************************************
    # * CMD - CHGLIBL
    # ***************************************
    cmd = XMLService::I_CMD.new("CHGLIBL LIBL(QTEMP #{$toolkit_test_lib})")
    # call IBM i
    cmd.xmlservice
    # inspect
    puts cmd.dump_inspect
	  # xmlservice error occurred?
    rc = cmd.xmlservice_error
	  if rc
      puts cmd.dump_error()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end
    # output
    # puts cmd.out_xml
    output = cmd.response.output
    puts output
    # verify
    must = "success"
    ok = output.include? must
    self.match_value(__method__,__LINE__,"E N D",must,output)
  end



  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
