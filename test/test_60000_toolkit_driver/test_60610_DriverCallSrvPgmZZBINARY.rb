# File: test_60610_DriverCallSrvPgmZZBINARY.rb
require "../test_authorization/auth"
require "../test_data/rowcol"

class Test_60610_DriverCallSrvPgmZZBINARY < RowColUnitTest
  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  #      *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  #      * zzbinary: check return binary 
  #      *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  #     P zzbinary        B                   export
  #     D zzbinary        PI            20A
  #     D  myBinary                     20A
  def test_0020_pgm_zzbinary

    mypack = XMLService::I_p.new('mypack',12,2,99.9)

    mybin = 0xA1A2A3A4A5A6A7A8A9AAA1A2A3A4A5A6A7A8A9AA
    zzbinary = XMLService::I_SRVPGM.new("ZZSRV","ZZBINARY",$toolkit_test_lib)
    zzbinary.inputParameter("parm","io",XMLService::I_Binary.new('myIn',20,mybin))
    zzbinary.setReturn("binary", XMLService::I_Binary.new('myOut',20,0x00))
    # call IBM i
    zzbinary.xmlservice
    # xmlservice error occurred?
    rc = zzbinary.xmlservice_error
    if rc
      puts zzbinary.dump_all()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts zzbinary.dump_inspect

    # output
    puts "\n+++++++++ response output ++++++++++++++++"
    puts "#{zzbinary.name}.#{zzbinary.func}:"
    puts "Input....#{zzbinary.response.myIn}"

    # return data
    puts "\n+++++++++ returndata output ++++++++++++++++"
    puts "Return...#{zzbinary.returndata.myOut}"

    # verify
    self.match_value(__method__,__LINE__,'myOut',mybin.to_s(16).upcase,zzbinary.returndata.myOut.to_s)
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
