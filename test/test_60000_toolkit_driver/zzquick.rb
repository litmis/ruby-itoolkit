# File: test_Quick.rb
require "../test_authorization/auth"
require "../test_data/rowcol"

class Test_Quick < RowColUnitTest


  def test_0500_DB2_query_parm2
    # ***************************************
    # * DB2 - query
    # ***************************************
    id = 5
    weight = 0.20
    likethis = 'Peaches%'   # ibm_db driver
    query = XMLService::I_DB2.new("select * from #{$toolkit_test_db2_lib}/animals where NAME LIKE ?",{'name'=>likethis},{'error'=>'on'})
    # call IBM i
    puts "\n+++++++++ input xml ++++++++++++++++"
    puts query.to_xml
    query.xmlservice
    # xmlservice error occurred?
    rc = query.xmlservice_error
    if rc
      puts query.dump_all()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts query.dump_inspect

    # output
    puts "\n+++++++++ response output ++++++++++++++++"
    puts query.out_xml

  end


  def no_test_0500_pgm_zzcall_array
    # call IBM i ZZCALL (default values)
    zzcall = XMLService::I_PGM.new("ZZCALL",$toolkit_test_lib,{'error'=>'fast'}) 
    zzcall << XMLService::I_a.new('inchara',1,'s')
    zzcall << XMLService::I_a.new('incharb',1,'r') 
    zzcall << XMLService::I_p.new('indec1',7,4,55.1111) 
    zzcall << XMLService::I_p.new('indec2',12,2,333.22)
    zzcall << XMLService::I_DS.new('inds1',1,
                                   [{
                                     'fred'=>XMLService::I_a.new('dschara',1,'t'),
                                     'wilma'=>XMLService::I_a.new('dscharb',1,'f'),
                                     'betty'=>XMLService::I_p.new('dsdec1',7,4,77.6666),
                                     'bambam'=>XMLService::I_p.new('dsdec2',12,2,88888.77)
                                    }])

    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts zzcall.dump_inspect

    zzcall.input.inchara          = '1'
    zzcall.input.incharb          = '1'
    zzcall.input.indec1           = 11.1111
    zzcall.input.indec2           = 11.11
    zzcall.input.inds1[0]['fred'].dschara = '1'
    zzcall.input.inds1[0]['wilma'].dscharb = '1'
    zzcall.input.inds1[0]['betty'].dsdec1  = 111.1111
    zzcall.input.inds1[0]['bambam'].dsdec2  = 1111.11

    parms = [ 'inchara'=>'2', 'incharb'=>'2', 'indec1'=>22.2222, 'indec2'=>22.22, 
              'inds1'=>[{
                'wilma'=>{'dscharb'=>'2'},
                'betty'=>{'dsdec1'=>222.2222}}]
            ]
    zzcall.call(parms)

    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts zzcall.dump_inspect

    puts "\n+++++++++ dump input xml ++++++++++++++++"
    puts zzcall.to_xml

    puts "\n+++++++++ output ++++++++++++++++"
    puts " inchara...#{zzcall.response.inchara}\n"
    puts " incharb...#{zzcall.response.incharb}\n"
    puts " indec1....#{zzcall.response.indec1}\n"
    puts " indec2....#{zzcall.response.indec2}\n"
    zzcall.response.inds1.each do |inds1|
      puts "  dschara...#{inds1.dschara}"
      puts "  dscharb...#{inds1.dscharb}"
      puts "  dsdec1....#{inds1.dsdec1}"
      puts "  dsdec2....#{inds1.dsdec2}"
    end
  end

  def no_test_0501_pgm_zzcall_array
    zzcall << XMLService::I_DS.new('inds1',1,
                                   [ 
                                     XMLService::I_a.new('dschara',1,'t'),
                                     XMLService::I_a.new('dscharb',1,'f'),
                                     XMLService::I_p.new('dsdec1',7,4,77.6666),
                                     XMLService::I_p.new('dsdec2',12,2,88888.77)
                                    ])
    zzcall.input.inds1[0].dschara = 'a'
    zzcall.input.inds1[1].dscharb = 'a'
    zzcall.input.inds1[2].dsdec1  = 111.1111
    zzcall.input.inds1[3].dsdec2  = 1111.11
  end
  def no_test_0502_pgm_zzcall_array
    zzcall << XMLService::I_DS.new('inds1',1,
                                   {
                                     'dschara'=>XMLService::I_a.new('dschara',1,'t'),
                                     'dscharb'=>XMLService::I_a.new('dscharb',1,'f'),
                                     'dsdec1'=>XMLService::I_p.new('dsdec1',7,4,77.6666),
                                     'dsdec2'=>XMLService::I_p.new('dsdec2',12,2,88888.77)
                                    })
    zzcall.input.inds1['dschara'].dschara = 'a'
    zzcall.input.inds1['dscharb'].dscharb = 'a'
    zzcall.input.inds1['dsdec1'].dsdec1  = 111.1111
    zzcall.input.inds1['dsdec2'].dsdec2  = 1111.11
  end


  def no_test_0503_pgm_zzcall_array
    zzcall << XMLService::I_DS.new('inds1',1,
                                   [{
                                     'dschara'=>XMLService::I_a.new('dschara',1,'t'),
                                     'dscharb'=>XMLService::I_a.new('dscharb',1,'f'),
                                     'dsdec1'=>XMLService::I_p.new('dsdec1',7,4,77.6666),
                                     'dsdec2'=>XMLService::I_p.new('dsdec2',12,2,88888.77)
                                    }])

    zzcall.input.inds1[0]['dschara'].dschara = 'a'
    zzcall.input.inds1[0]['dscharb'].dscharb = 'a'
    zzcall.input.inds1[0]['dsdec1'].dsdec1  = 111.1111
    zzcall.input.inds1[0]['dsdec2'].dsdec2  = 1111.11
  end


  def no_test_0504_pgm_zzcall_array
    zzcall << XMLService::I_DS.new('inds1',1,
                                   [[
                                     XMLService::I_a.new('dschara',1,'t'),
                                     XMLService::I_a.new('dscharb',1,'f'),
                                     XMLService::I_p.new('dsdec1',7,4,77.6666),
                                     XMLService::I_p.new('dsdec2',12,2,88888.77)
                                    ]])
    zzcall.input.inds1[0][0].dschara = 'a'
    zzcall.input.inds1[0][1].dscharb = 'a'
    zzcall.input.inds1[0][2].dsdec1  = 111.1111
    zzcall.input.inds1[0][3].dsdec2  = 1111.11
  end

end
