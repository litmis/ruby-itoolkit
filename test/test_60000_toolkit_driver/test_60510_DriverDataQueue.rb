# File: test_60510_DriverDataQueue.rb
require "../test_authorization/auth"
require "../test_data/rowcol"

class Test_60510_DriverDataQueue < RowColUnitTest

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0020_data_queue
    # ***************************************
    # * CMD - Recreate Data Queue (DLTDTAQ, CRTDTAQ)
    # ***************************************
    dltdatq = XMLService::I_CMD.new("DLTDTAQ DTAQ(#{$toolkit_test_lib}/MYDATAQ)")
    # call IBM i
    dltdatq.xmlservice
	  # xmlservice error occurred?
    rc = dltdatq.xmlservice_error
	  if rc
      puts dltdatq.dump_all()
      # self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts dltdatq.dump_inspect

    # output
    puts "\n+++++++++ response output ++++++++++++++++"
    puts dltdatq.response.output

    crtdatq = XMLService::I_CMD.new("CRTDTAQ DTAQ(#{$toolkit_test_lib}/MYDATAQ) MAXLEN(100) AUT(*EXCLUDE)")
    # call IBM i
    crtdatq.xmlservice
	  # xmlservice error occurred?
    rc = crtdatq.xmlservice_error
    if rc
      puts crtdatq.dump_all()
      # self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts crtdatq.dump_inspect

    # output
    puts "\n+++++++++ response output ++++++++++++++++"
    puts crtdatq.response.output

    # ***************************************
    # * PGM - Send Data Queue (QSNDDTAQ) API
    # ***************************************
    # * 1 Data queue name     Input Char(10)
    # * 2 Library name        Input Char(10)
    # * 3 Length of data      Input Packed(5,0)
    # * 4 Data Input Char(*)  Input
    messageSnd = 'System i data queues forever'
    qsnddtaq = XMLService::I_PGM.new("QSNDDTAQ")
    qsnddtaq << XMLService::I_a.new('queueName',10,'MYDATAQ')
    qsnddtaq << XMLService::I_a.new('libName',10,$toolkit_test_lib)
    qsnddtaq << XMLService::I_p.new('lenData',5,0,50.0)
    qsnddtaq << XMLService::I_a.new('dataInput',100,messageSnd)
    # call IBM i
    qsnddtaq.xmlservice
    # xmlservice error occurred?
    rc = qsnddtaq.xmlservice_error
    if rc
      puts qsnddtaq.dump_all()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end
    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts qsnddtaq.dump_inspect

    # output
    puts "\n+++++++++ response output ++++++++++++++++"
    puts qsnddtaq.response.output
    puts "Send......#{qsnddtaq.name}: #{qsnddtaq.response.dataInput}"

    # ***************************************
    # * PGM - Receive Data Queue (QRCVDTAQ) API
    # ***************************************
    # * 1 Data queue name  Input Char(10)
    # * 2 Library name     Input Char(10)
    # * 3 Length of data   Input Packed(5,0)
    # * 4 Data Char(*)     Output
    # * 5 Wait time        Input Packed(5,0)
    qrcvdtaq = XMLService::I_PGM.new("QRCVDTAQ")
    qrcvdtaq << XMLService::I_a.new('queueName',10,'MYDATAQ')
    qrcvdtaq << XMLService::I_a.new('libName',10,$toolkit_test_lib)
    qrcvdtaq << XMLService::I_p.new('lenData',5,0,50.0)
    qrcvdtaq << XMLService::I_a.new('dataOutput',100,'replace stuff here')
    qrcvdtaq << XMLService::I_p.new('waitTime',5,0,0.0)
    # call IBM i
    qrcvdtaq.xmlservice
    # xmlservice error occurred?
    rc = qrcvdtaq.xmlservice_error
    if rc
      puts qrcvdtaq.dump_all()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts qrcvdtaq.dump_inspect

    # output
    puts "\n+++++++++ response output ++++++++++++++++"
    puts qrcvdtaq.response.output
    puts "Receive...#{qrcvdtaq.name}: #{qrcvdtaq.response.dataOutput}"
    # verify
    self.match_value(__method__,__LINE__,'dataOutput' ,messageSnd ,qrcvdtaq.response.dataOutput)

  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
