# File: test_65010_DriverCallDB2.rb
require "../test_authorization/auth"
require "../test_data/rowcol"

class Test_65010_DriverCallDB2 < RowColUnitTest
  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0020_DB2_query
    # ***************************************
    # * DB2 - query
    # ***************************************
    query = XMLService::I_DB2.new("select * from #{$toolkit_test_db2_lib}/animals")
    # call IBM i
    puts "\n+++++++++ input xml ++++++++++++++++"
    puts query.to_xml
    query.xmlservice
    # xmlservice error occurred?
    rc = query.xmlservice_error
    if rc
      puts query.dump_all()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts query.dump_inspect

    # output
    # puts query.out_xml
    # rows
    puts "\n+++++++++ response output ++++++++++++++++"
    query.response.output.each do |row|
      all = ""
      row.each do |n,v|
        all << "#{n}=#{v} "
      end
      puts all
    end

    # verify
    test = "ID=0 BREED=cat NAME=Pook WEIGHT=3.20"
    all = ""
    query.response.output.each do |row|
      row.each do |n,v|
        all << "#{n}=#{v} "
      end
      break
    end
    self.match_value(__method__,__LINE__,'one row',test,all)
  end

  def test_0030_DB2_query_parm
    # ***************************************
    # * DB2 - query
    # ***************************************
    id = 1
    query = XMLService::I_DB2.new("select * from #{$toolkit_test_db2_lib}/animals where ID < ?",{'id'=>id})
    # call IBM i
    puts "\n+++++++++ input xml ++++++++++++++++"
    puts query.to_xml
    query.xmlservice
    # xmlservice error occurred?
    rc = query.xmlservice_error
    if rc
      puts query.dump_all()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts query.dump_inspect

    # output
    puts "\n+++++++++ response output ++++++++++++++++"
    # puts query.out_xml
    # rows
    puts "id < #{query.returndata.id.strip}"
    query.response.output.each do |row|
      all = ""
      row.each do |n,v|
        all << "#{n}=#{v} "
      end
      puts all
    end

    # verify
    test = "ID=0 BREED=cat NAME=Pook WEIGHT=3.20"
    all = ""
    query.response.output.each do |row|
      row.each do |n,v|
        all << "#{n}=#{v} "
      end
      break
    end
    self.match_value(__method__,__LINE__,'one row',test,all)
    self.match_value(__method__,__LINE__,'parm id',id,query.returndata.id)
  end

  def test_0040_DB2_query_parm2
    # ***************************************
    # * DB2 - query
    # ***************************************
    id = 5
    weight = 0.20
    query = XMLService::I_DB2.new("select * from #{$toolkit_test_db2_lib}/animals where ID < ? and WEIGHT > ?",{'id'=>id,"weight"=>weight})
    # call IBM i
    puts "\n+++++++++ input xml ++++++++++++++++"
    puts query.to_xml
    query.xmlservice
    # xmlservice error occurred?
    rc = query.xmlservice_error
    if rc
      puts query.dump_all()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts query.dump_inspect

    # output
    puts "\n+++++++++ response output ++++++++++++++++"
    # puts query.out_xml
    # rows
    puts "id < #{query.returndata.id.strip}"
    puts "weight > #{query.returndata.weight.strip}"
    query.response.output.each do |row|
      all = ""
      row.each do |n,v|
        all << "#{n}=#{v} "
      end
      puts all
    end

    # verify
    test = "ID=0 BREED=cat NAME=Pook WEIGHT=3.20"
    all = ""
    query.response.output.each do |row|
      row.each do |n,v|
        all << "#{n}=#{v} "
      end
      break
    end
    self.match_value(__method__,__LINE__,'one row',test,all)
    self.match_value(__method__,__LINE__,'parm id',id,query.returndata.id)
    self.match_value(__method__,__LINE__,'parm weight',weight,query.returndata.weight)
  end


  def test_0050_DB2_procedure
    # ***************************************
    # * DB2 - query
    # ***************************************
    in_name1 = 'Peaches'
    in_name2 = 'Rickety Ride'
    in_weight = 22.22
    query = XMLService::I_DB2.new("call #{$toolkit_test_db2_lib}/MATCH_ANIMAL(?, ?, ?)",
      {'name1'=>in_name1,"name2"=>in_name2,"weight"=>in_weight})
    # call IBM i
    puts "\n+++++++++ input xml ++++++++++++++++"
    puts query.to_xml
    query.xmlservice
    # xmlservice error occurred?
    rc = query.xmlservice_error
    if rc
      puts query.dump_all()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts query.dump_inspect

    # output
    puts "\n+++++++++ response output ++++++++++++++++"
    # puts query.out_xml
    # rows
    puts "name1 = #{query.returndata.name1.strip}"
    puts "name2 = #{query.returndata.name2.strip}"
    puts "weight = #{query.returndata.weight.strip}"
    query.response.output.each do |row|
      all = ""
      row.each do |n,v|
        all << "#{n}=#{v} "
      end
      puts all
    end

    # verify
    out_name1 = 'Peaches'
    out_name2 = 'TRUE'
    out_weight = 12.3
    test = "NAME=Peaches BREED=dog WEIGHT=12.30"
    all = ""
    query.response.output.each do |row|
      row.each do |n,v|
        all << "#{n}=#{v} "
      end
      break
    end
    self.match_value(__method__,__LINE__,'one row',test,all)
    self.match_value(__method__,__LINE__,'name1',out_name1,query.returndata.name1)
    self.match_value(__method__,__LINE__,'name2',out_name2,query.returndata.name2)
    self.match_value(__method__,__LINE__,'name2',out_weight,query.returndata.weight)
  end


  def test_0100_DB2_query_graphic
    # ***************************************
    # * DB2 - query
    # ***************************************
    query = XMLService::I_DB2.new("select * from #{$toolkit_test_db2_lib}/VAR_GRAPHIC_TEST")
    # call IBM i
    puts "\n+++++++++ input xml ++++++++++++++++"
    puts query.to_xml
    query.xmlservice
    # xmlservice error occurred?
    rc = query.xmlservice_error
    if rc
      puts query.dump_all()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts query.dump_inspect

    # output
    puts "\n+++++++++ response output ++++++++++++++++"
    # puts query.out_xml
    # rows
    query.response.output.each do |row|
      all = ""
      row.each do |n,v|
        all << "#{n}=#{v} "
      end
      puts all
    end

    # verify
    test = "COL1=something COL2=something"
    all = ""
    query.response.output.each do |row|
      row.each do |n,v|
        all << "#{n}=#{v} "
      end
      break
    end
    self.match_value(__method__,__LINE__,'one row',test,all)
  end



  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
