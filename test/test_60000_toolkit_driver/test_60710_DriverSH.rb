# File: test_60710_DriverSH.rb
require "../test_authorization/auth"
require "../test_data/rowcol"

class Test_60710_DriverSH < RowColUnitTest
  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0020_sh_system_wrksysval
    # ***************************************
    # * PASE shell utility system - WRKSYSVAL
    # ***************************************
    wrksysval = XMLService::I_SH.new("system -i 'WRKSYSVAL OUTPUT(*PRINT)'")
    # call IBM i
    wrksysval.xmlservice

    # inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts wrksysval.dump_inspect

    # output
    puts "\n+++++++++ response output ++++++++++++++++"
    # puts wrksysval.response.output.class.name
    output = wrksysval.response.output
    puts output

    # verify
    must = "E N D  O F  L I S T I N G"
    ok = output.include? must
    self.match_value(__method__,__LINE__,"E N D",must,output)
  end


  def test_0030_sh_system_dsplibl
    # ***************************************
    # * PASE shell utility system - dsplibl
    # ***************************************
    dsplibl = XMLService::I_SH.new("system -i 'dsplibl OUTPUT(*PRINT)'", {'rows'=>'on'})
    for i in 0..5
      # call IBM i
      dsplibl.xmlservice

      # inspect
      puts "\n+++++++++ dump_inspect ++++++++++++++++"
      puts dsplibl.dump_inspect

      # output
      # puts dsplibl.out_xml
      # puts dsplibl.response.output.class.name
      puts "\n+++++++++ response output ++++++++++++++++"
      output = dsplibl.response.output
      for i in 0..output.count-1
        puts "line #{i}: #{output[i]}"
      end

      # verify
      must = "E N D  O F  L I S T I N G"
      ok = output.include? must
      self.match_value(__method__,__LINE__,"E N D",must,output)
    end
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
