# File: test_60010_DriverCallPgmZZCALL.rb
require "../test_authorization/auth"
require "../test_data/rowcol"

#   D  INCHARA        S              1a
#   D  INCHARB        S              1a
#   D  INDEC1         S              7p 4        
#   D  INDEC2         S             12p 2
#   D  INDS1          DS                  
#   D   DSCHARA                      1a
#   D   DSCHARB                      1a           
#   D   DSDEC1                       7p 4      
#   D   DSDEC2                      12p 2            
#    *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#    * main(): Control flow
#    *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#   C     *Entry        PLIST                   
#   C                   PARM                    INCHARA
#   C                   PARM                    INCHARB
#   C                   PARM                    INDEC1
#   C                   PARM                    INDEC2
#   C                   PARM                    INDS1

class Test_60010_DriverCallPgmZZCALL < RowColUnitTest
  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------
  def test_0030_pgm_zzcall_array
    # call IBM i ZZCALL (default values)
    zzcall = XMLService::I_PGM.new("ZZCALL",$toolkit_test_lib,{'error'=>'fast'}) 
    zzcall << XMLService::I_a.new('inchara',1,'s')
    zzcall << XMLService::I_a.new('incharb',1,'r') 
    zzcall << XMLService::I_p.new('indec1',7,4,55.1111) 
    zzcall << XMLService::I_p.new('indec2',12,2,333.22)
    zzcall << XMLService::I_DS.new('inds1',1,
                                   [ 
                                    XMLService::I_a.new('dschara',1,'t'),
                                    XMLService::I_a.new('dscharb',1,'f'),
                                    XMLService::I_p.new('dsdec1',7,4,77.6666),
                                    XMLService::I_p.new('dsdec2',12,2,88888.77)
                                   ])


    # set input variables for call ... normal Ruby OO object path
    zzcall.input.PARM0.inchara          = 'a'
    zzcall.input.PARM1.incharb          = 'a'
    zzcall.input.PARM2.indec1           = 11.1111
    zzcall.input.PARM3.indec2           = 11.11
    zzcall.input.PARM4.inds1[0].dschara = 'a'
    zzcall.input.PARM4.inds1[1].dscharb = 'a'
    zzcall.input.PARM4.inds1[2].dsdec1  = 111.1111
    zzcall.input.PARM4.inds1[3].dsdec2  = 1111.11
    puts "\n+++++++++ input normal ++++++++++++++++"
    puts zzcall.to_xml
    # verify input
    self.match_value(__method__,__LINE__,'inchara' ,'a'        ,zzcall.input.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'a'        ,zzcall.input.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,11.1111    ,zzcall.input.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,11.11      ,zzcall.input.PARM3.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'a'        ,zzcall.input.PARM4.inds1[0].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'a'        ,zzcall.input.PARM4.inds1[1].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,111.1111   ,zzcall.input.PARM4.inds1[2].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,1111.11    ,zzcall.input.PARM4.inds1[3].dsdec2)

    # set input with fewer dots ... clever naming
    zzcall.input.inchara          = 'b'
    zzcall.input.incharb          = 'b'
    zzcall.input.indec1           = 22.2222
    zzcall.input.indec2           = 22.22
    zzcall.input.inds1[0].dschara = 'b'
    zzcall.input.inds1[1].dscharb = 'b'
    zzcall.input.inds1[2].dsdec1  = 222.2222
    zzcall.input.inds1[3].dsdec2  = 2222.22
    puts "\n+++++++++ input fewer ++++++++++++++++"
    puts zzcall.to_xml
    # verify input
    self.match_value(__method__,__LINE__,'inchara' ,'b'        ,zzcall.input.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'b'        ,zzcall.input.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,22.2222    ,zzcall.input.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,22.22      ,zzcall.input.PARM3.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'b'        ,zzcall.input.PARM4.inds1[0].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'b'        ,zzcall.input.PARM4.inds1[1].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,222.2222   ,zzcall.input.PARM4.inds1[2].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,2222.22    ,zzcall.input.PARM4.inds1[3].dsdec2)


    # call IBM i ZZCALL
    zzcall.execute

    # xmlservice error occurred?
    rc = zzcall.xmlservice_error
    if rc
      puts zzcall.dump_error
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    #puts zzcall.inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts zzcall.dump_inspect

    puts "\n+++++++++ output xml ++++++++++++++++"
    puts zzcall.out_xml

    # response xml  ... normal Ruby OO object path
    puts "\n+++++++++ output normal ++++++++++++++++"
    puts " inchara...#{zzcall.response.PARM0.inchara}\n"
    puts " incharb...#{zzcall.response.PARM1.incharb}\n"
    puts " indec1....#{zzcall.response.PARM2.indec1}\n"
    puts " indec2....#{zzcall.response.PARM3.indec2}\n"
    for i in 0..zzcall.response.PARM4.inds1.count-1
      puts " inds1[#{i}] = ["
      puts "  dschara...#{zzcall.response.PARM4.inds1[i].dschara}"
      puts "  dscharb...#{zzcall.response.PARM4.inds1[i].dscharb}"
      puts "  dsdec1....#{zzcall.response.PARM4.inds1[i].dsdec1}"
      puts "  dsdec2....#{zzcall.response.PARM4.inds1[i].dsdec2}"
      puts " ]\n"
    end
    # verify response
    self.match_value(__method__,__LINE__,'inchara' ,'C'             ,zzcall.response.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'D'             ,zzcall.response.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,321.1234        ,zzcall.response.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,1234567890.12   ,zzcall.response.PARM3.indec2)
    for i in 0..zzcall.response.PARM4.inds1.count - 1
      self.match_value(__method__,__LINE__,'dschara' ,'E'           ,zzcall.response.PARM4.inds1[i].dschara)
      self.match_value(__method__,__LINE__,'dscharb' ,'F'           ,zzcall.response.PARM4.inds1[i].dscharb)
      self.match_value(__method__,__LINE__,'dsdec1'  ,333.333       ,zzcall.response.PARM4.inds1[i].dsdec1)
      self.match_value(__method__,__LINE__,'dsdec2'  ,4444444444.44 ,zzcall.response.PARM4.inds1[i].dsdec2)
    end

    # response xml with fewer dots ... clever naming
    puts "\n+++++++++ output fewer ++++++++++++++++"
    puts " inchara...#{zzcall.response.inchara}\n"
    puts " incharb...#{zzcall.response.incharb}\n"
    puts " indec1....#{zzcall.response.indec1}\n"
    puts " indec2....#{zzcall.response.indec2}\n"
    zzcall.response.inds1.each do |inds1|
      puts "  dschara...#{inds1.dschara}"
      puts "  dscharb...#{inds1.dscharb}"
      puts "  dsdec1....#{inds1.dsdec1}"
      puts "  dsdec2....#{inds1.dsdec2}"
    end
    # verify response
    self.match_value(__method__,__LINE__,'inchara' ,'C'             ,zzcall.response.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'D'             ,zzcall.response.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,321.1234        ,zzcall.response.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,1234567890.12   ,zzcall.response.indec2)
    zzcall.response.inds1.each do |inds1|
      self.match_value(__method__,__LINE__,'dschara' ,'E'           ,inds1.dschara)
      self.match_value(__method__,__LINE__,'dscharb' ,'F'           ,inds1.dscharb)
      self.match_value(__method__,__LINE__,'dsdec1'  ,333.333       ,inds1.dsdec1)
      self.match_value(__method__,__LINE__,'dsdec2'  ,4444444444.44 ,inds1.dsdec2)
    end


  end

  def test_0040_pgm_zzcall_hash
    # call IBM i ZZCALL (default values)
    zzcall = XMLService::I_PGM.new("ZZCALL",$toolkit_test_lib,{'error'=>'fast'})
    zzcall.inputParameter('willy','both',XMLService::I_a.new('inchara',1,'s'))
    zzcall.inputParameter('nilly','both',XMLService::I_a.new('incharb',1,'r'))
    zzcall.inputParameter('silly','both',XMLService::I_p.new('indec1',7,4,55.1111))
    zzcall.inputParameter('tilly','both',XMLService::I_p.new('indec2',12,2,333.22))
    zzcall.inputParameter('filly','both',XMLService::I_DS.new('inds1',1,
                                   { 
                                    'dschara'=>XMLService::I_a.new('dschara',1,'t'),
                                    'dscharb'=>XMLService::I_a.new('dscharb',1,'f'),
                                    'dsdec1'=>XMLService::I_p.new('dsdec1',7,4,77.6666),
                                    'dsdec2'=>XMLService::I_p.new('dsdec2',12,2,88888.77)
                                   }))

    # set input variables for call ... normal Ruby OO object path
    zzcall.input.willy.inchara = 'a'
    zzcall.input.nilly.incharb = 'a'
    zzcall.input.silly.indec1  = 11.1111
    zzcall.input.tilly.indec2  = 11.11
    zzcall.input.filly.inds1['dschara'].dschara = 'a'
    zzcall.input.filly.inds1['dscharb'].dscharb = 'a'
    zzcall.input.filly.inds1['dsdec1'].dsdec1 = 111.1111
    zzcall.input.filly.inds1['dsdec2'].dsdec2 = 111.11
    puts "\n+++++++++ input normal ++++++++++++++++"
    puts zzcall.to_xml
    self.match_value(__method__,__LINE__,'inchara' ,'a'           ,zzcall.input.willy.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'a'           ,zzcall.input.nilly.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,11.1111       ,zzcall.input.silly.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,11.11         ,zzcall.input.tilly.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'a'           ,zzcall.input.filly.inds1['dschara'].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'a'           ,zzcall.input.filly.inds1['dscharb'].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,111.1111      ,zzcall.input.filly.inds1['dsdec1'].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,111.11        ,zzcall.input.filly.inds1['dsdec2'].dsdec2)


    # set input with fewer dots ... clever naming
    zzcall.input.inchara = 'b'
    zzcall.input.incharb = 'b'
    zzcall.input.indec1  = 22.2222
    zzcall.input.indec2  = 22.22
    zzcall.input.inds1['dschara'].dschara = 'b'
    zzcall.input.inds1['dscharb'].dscharb = 'b'
    zzcall.input.inds1['dsdec1'].dsdec1 = 222.2222
    zzcall.input.inds1['dsdec2'].dsdec2 = 222.22
    puts "\n+++++++++ input fewer ++++++++++++++++"
    puts zzcall.to_xml
    self.match_value(__method__,__LINE__,'inchara' ,'b'           ,zzcall.input.willy.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'b'           ,zzcall.input.nilly.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,22.2222       ,zzcall.input.silly.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,22.22         ,zzcall.input.tilly.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'b'           ,zzcall.input.filly.inds1['dschara'].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'b'           ,zzcall.input.filly.inds1['dscharb'].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,222.2222      ,zzcall.input.filly.inds1['dsdec1'].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,222.22        ,zzcall.input.filly.inds1['dsdec2'].dsdec2)

    # call IBM i ZZCALL
    zzcall.execute

    # xmlservice error occurred?
    rc = zzcall.xmlservice_error
    if rc
      puts zzcall.dump_error()
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    #puts zzcall.inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts zzcall.dump_inspect

    puts "\n+++++++++ output xml ++++++++++++++++"
    puts zzcall.out_xml


    # response xml  ... normal Ruby OO object path
    puts "\n+++++++++ output normal ++++++++++++++++"
    puts " inchara...#{zzcall.response.willy.inchara}\n"
    puts " incharb...#{zzcall.response.nilly.incharb}\n"
    puts " indec1....#{zzcall.response.silly.indec1}\n"
    puts " indec2....#{zzcall.response.tilly.indec2}\n"
    zzcall.response.filly.inds1.each do |inds1|
      puts "  dschara...#{inds1.dschara}"
      puts "  dscharb...#{inds1.dscharb}"
      puts "  dsdec1....#{inds1.dsdec1}"
      puts "  dsdec2....#{inds1.dsdec2}"
    end
    # verify output
    self.match_value(__method__,__LINE__,'inchara' ,'C'           ,zzcall.response.willy.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'D'           ,zzcall.response.nilly.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,321.1234      ,zzcall.response.silly.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,1234567890.12 ,zzcall.response.tilly.indec2)
    zzcall.response.filly.inds1.each do |inds1|
      self.match_value(__method__,__LINE__,'dschara' ,'E'           ,inds1.dschara)
      self.match_value(__method__,__LINE__,'dscharb' ,'F'           ,inds1.dscharb)
      self.match_value(__method__,__LINE__,'dsdec1'  ,333.333       ,inds1.dsdec1)
      self.match_value(__method__,__LINE__,'dsdec2'  ,4444444444.44 ,inds1.dsdec2)
    end

    # response xml with fewer dots ... clever naming
    puts "\n+++++++++ output fewer ++++++++++++++++"
    puts " inchara...#{zzcall.response.inchara}\n"
    puts " incharb...#{zzcall.response.incharb}\n"
    puts " indec1....#{zzcall.response.indec1}\n"
    puts " indec2....#{zzcall.response.indec2}\n"
    zzcall.response.inds1.each do |inds1|
      puts "  dschara...#{inds1.dschara}"
      puts "  dscharb...#{inds1.dscharb}"
      puts "  dsdec1....#{inds1.dsdec1}"
      puts "  dsdec2....#{inds1.dsdec2}"
    end
    # verify output
    self.match_value(__method__,__LINE__,'inchara' ,'C'           ,zzcall.response.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'D'           ,zzcall.response.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,321.1234      ,zzcall.response.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,1234567890.12 ,zzcall.response.indec2)
    zzcall.response.inds1.each do |inds1|
      self.match_value(__method__,__LINE__,'dschara' ,'E'           ,inds1.dschara)
      self.match_value(__method__,__LINE__,'dscharb' ,'F'           ,inds1.dscharb)
      self.match_value(__method__,__LINE__,'dsdec1'  ,333.333       ,inds1.dsdec1)
      self.match_value(__method__,__LINE__,'dsdec2'  ,4444444444.44 ,inds1.dsdec2)
    end
  end


  def test_0130_pgm_zzcall_array
    # call IBM i ZZCALL (default values)
    zzcall = XMLService::I_PGM.new("ZZCALL",$toolkit_test_lib,{'error'=>'fast'}) 
    zzcall << XMLService::I_a.new('inchara',1,'s')
    zzcall << XMLService::I_a.new('incharb',1,'r') 
    zzcall << XMLService::I_p.new('indec1',7,4,55.1111) 
    zzcall << XMLService::I_p.new('indec2',12,2,333.22)
    zzcall << XMLService::I_DS.new('inds1',1,
                                   [ 
                                    XMLService::I_a.new('dschara',1,'t'),
                                    XMLService::I_a.new('dscharb',1,'f'),
                                    XMLService::I_p.new('dsdec1',7,4,77.6666),
                                    XMLService::I_p.new('dsdec2',12,2,88888.77)
                                   ])

    puts "\n+++++++++ input call array of hash params ++++++++++++++++"
    parms = [ {'inchara'=>'1'},
              {'incharb'=>'1'},
              {'indec1'=>11.1111},
              {'indec2'=>111.11},
              {'inds1'=>[{'dschara'=>'2'},{'dscharb'=>'2'},{'dsdec1'=>22.2222},{'dsdec2'=>222.22}]} ]
    zzcall.call(parms)
    puts zzcall.to_xml
    self.match_value(__method__,__LINE__,'inchara' ,'1'        ,zzcall.input.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'1'        ,zzcall.input.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,11.1111    ,zzcall.input.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,111.11     ,zzcall.input.PARM3.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'2'        ,zzcall.input.PARM4.inds1[0].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'2'        ,zzcall.input.PARM4.inds1[1].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,22.2222   ,zzcall.input.PARM4.inds1[2].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,222.22    ,zzcall.input.PARM4.inds1[3].dsdec2)


    puts "\n+++++++++ input call individual hash params ++++++++++++++++"
    zzcall.call(
      {'inchara'=>'3'},
      {'incharb'=>'3'},
      {'indec1'=>33.3333},
      {'indec2'=>333.33},
      {'inds1'=>[{'dschara'=>'4'},{'dscharb'=>'4'},{'dsdec1'=>44.4444},{'dsdec2'=>444.44}]})
    puts zzcall.to_xml
    self.match_value(__method__,__LINE__,'inchara' ,'3'        ,zzcall.input.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'3'        ,zzcall.input.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,33.3333    ,zzcall.input.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,333.33     ,zzcall.input.PARM3.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'4'        ,zzcall.input.PARM4.inds1[0].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'4'        ,zzcall.input.PARM4.inds1[1].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,44.4444   ,zzcall.input.PARM4.inds1[2].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,444.44    ,zzcall.input.PARM4.inds1[3].dsdec2)


    puts "\n+++++++++ input call individual hash params (set one parm) ++++++++++++++++"
    zzcall.call({'indec1'=>55.5555})
    puts zzcall.to_xml
    self.match_value(__method__,__LINE__,'inchara' ,'3'        ,zzcall.input.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'3'        ,zzcall.input.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,55.5555    ,zzcall.input.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,333.33     ,zzcall.input.PARM3.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'4'        ,zzcall.input.PARM4.inds1[0].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'4'        ,zzcall.input.PARM4.inds1[1].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,44.4444   ,zzcall.input.PARM4.inds1[2].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,444.44    ,zzcall.input.PARM4.inds1[3].dsdec2)


    puts "\n+++++++++ input call individual hash params (set parm DS) ++++++++++++++++"
    zzcall.call({'inds1'=>[{'dschara'=>'5'},{'dscharb'=>'5'},{'dsdec1'=>55.5555},{'dsdec2'=>555.55}]})
    puts zzcall.to_xml
    self.match_value(__method__,__LINE__,'inchara' ,'3'        ,zzcall.input.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'3'        ,zzcall.input.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,55.5555    ,zzcall.input.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,333.33     ,zzcall.input.PARM3.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'5'        ,zzcall.input.PARM4.inds1[0].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'5'        ,zzcall.input.PARM4.inds1[1].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,55.5555   ,zzcall.input.PARM4.inds1[2].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,555.55    ,zzcall.input.PARM4.inds1[3].dsdec2)


    puts "\n+++++++++ input call array of hash params ++++++++++++++++"
    parms = [ 'inchara'=>'6',
              'incharb'=>'6',
              'indec1'=>66.6666,
              'indec2'=>666.66,
              'inds1'=>[{'dschara'=>'7'},{'dscharb'=>'7'},{'dsdec1'=>77.7777},{'dsdec2'=>777.77}] ]
    zzcall.call(parms)
    puts zzcall.to_xml
    self.match_value(__method__,__LINE__,'inchara' ,'6'        ,zzcall.input.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'6'        ,zzcall.input.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,66.6666    ,zzcall.input.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,666.66     ,zzcall.input.PARM3.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'7'        ,zzcall.input.PARM4.inds1[0].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'7'        ,zzcall.input.PARM4.inds1[1].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,77.7777   ,zzcall.input.PARM4.inds1[2].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,777.77    ,zzcall.input.PARM4.inds1[3].dsdec2)

  end

  def not_work_test_0000_pgm_zzcall_array
    puts "\n+++++++++ input call array of hash params ++++++++++++++++"
    parms = [ 'inchara'=>'8',
              'incharb'=>'8',
              'indec1'=>88.8888,
              'indec2'=>888.88,
              'inds1'=>[{'dschara'=>'9','dscharb'=>'9','dsdec1'=>99.9999,'dsdec2'=>999.99}] ]
    zzcall.call(parms)
    puts zzcall.to_xml
    self.match_value(__method__,__LINE__,'inchara' ,'8'        ,zzcall.input.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'8'        ,zzcall.input.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,88.9999    ,zzcall.input.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,888.99     ,zzcall.input.PARM3.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'9'        ,zzcall.input.PARM4.inds1[0].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'9'        ,zzcall.input.PARM4.inds1[1].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,99.9999    ,zzcall.input.PARM4.inds1[2].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,999.99     ,zzcall.input.PARM4.inds1[3].dsdec2)

  end

end

