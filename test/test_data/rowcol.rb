# test data for various tables
require "../test_authorization/auth"
require "test/unit"

class RowColUnitTest  < Test::Unit::TestCase
  def match_hash(test,linenbr,text,a,b)
    i = 0
    a.each do |key, value|
      if(b.has_key?(key))
        self.match_value(test,linenbr,"#{text} => #{key}",a[key],b[key])
      else
        k1 = key
        k2 = 'missing'
        assert_match( k2, k1, "#{test}:#{linenbr} mismatch: #{text}[#{k1}] == #{text}[]")
      end
    end 
  end
  def match_value(test,linenbr,text,a,b)
    # float/real/decimal/string,
    # you sir are a String/BigDecimal,
    # therefore you must float yourself, etc. ... 
    # Ruby perfection be my name (Borg),
    # perhaps yet another reason to use PHP
    case a
    when String
      case b
      when String
        # ok
      else
        b = b.to_s()
      end 
    when Integer
      case b
      when Integer
        # ok
      else
        b = b.to_i()
      end 
    when Float
      case b
      when Float
        # ok
      else
        b = b.to_f()
      end 
    else
    end 
    # make a check
    case a
    when String
      assert_match( a, b, "#{test}:#{linenbr} #{text} mismatch: #{a} == #{b}")
    else
      assert_equal( a, b, "#{test}:#{linenbr} #{text} mismatch: #{a} == #{b}")
    end
  end
end

