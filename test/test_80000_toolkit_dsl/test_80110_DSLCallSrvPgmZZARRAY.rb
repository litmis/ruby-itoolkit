# File: test_80110_DSLCallSrvPgmZZARRAY.rb
require "../test_authorization/auth"
require "../test_data/rowcol"

#     D ARRAYMAX        c                   const(999)
#     D dcRec_t         ds                  qualified based(Template)
#     D  dcMyName                     10A
#     D  dcMyJob                    4096A
#     D  dcMyRank                     10i 0
#     D  dcMyPay                      12p 2
#      *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#      * zzarray: check return array aggregate 
#      *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#     P zzarray         B                   export
#     D zzarray         PI                  likeds(dcRec_t) dim(ARRAYMAX)
#     D  myName                       10A
#     D  myMax                        10i 0
#     D  myCount                      10i 0
class Test_80110_DSLCallSrvPgmZZARRAY < RowColUnitTest
  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0050_zzarray
    count = 5
    zzwho = 'Ranger'
    zzarray = XMLService::I_SRVPGM.new("ZZSRV","ZZARRAY",$toolkit_test_lib,{'error'=>'on'})
    zzarray.input_parms do
     char "myName", 10
     long "myMax"
     long_enddo "myCount"
    end
    zzarray.return_parms do
     struct_dou "dcRec_t", 999, "myCount" do
       char "dcMyName", 10
       char "dcMyJob", 4096
       long "dcMyRank"
       dec "dcMyPay", 12, 2
     end
    end

    # set input variables for call
    zzarray.input.PARM0.myName = 'Skippy'
    zzarray.input.PARM1.myMax  = 9
    zzarray.input.PARM2.myCount= 0
    puts "\n+++++++++ input normal ++++++++++++++++"
    puts zzarray.to_xml
    # verify
    self.match_value(__method__,__LINE__,'myName'     ,'Skippy'    ,zzarray.input.PARM0.myName)
    self.match_value(__method__,__LINE__,'myMax'      ,9           ,zzarray.input.PARM1.myMax)
    self.match_value(__method__,__LINE__,'myCount'    ,0           ,zzarray.input.PARM2.myCount)


    # set input variables for call
    zzarray.input.myName = zzwho
    zzarray.input.myMax  = count
    zzarray.input.myCount= 0
    puts "\n+++++++++ input fewer ++++++++++++++++"
    puts zzarray.to_xml
    # verify
    self.match_value(__method__,__LINE__,'myName'     ,zzwho    ,zzarray.input.PARM0.myName)
    self.match_value(__method__,__LINE__,'myMax'      ,count    ,zzarray.input.PARM1.myMax)
    self.match_value(__method__,__LINE__,'myCount'    ,0        ,zzarray.input.PARM2.myCount)

    # call IBM i
    zzarray.call

    # puts zzarray.inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts zzarray.dump_inspect

	  # xmlservice error occurred?
    rc = zzarray.xmlservice_error
    if rc
      puts zzcall.dump_error
      self.match_value(__method__,__LINE__,'error' ,false,rc)
    end

    # output xml
    puts "\n+++++++++ output xml ++++++++++++++++"
    puts zzarray.out_xml


    # output
    puts "\n+++++++++ output ++++++++++++++++"
    puts "#{zzarray.name}.#{zzarray.func}:\n"
    puts " myName....#{zzarray.response.myName}\n"
    puts " myMax.....#{zzarray.response.myMax}\n"
    puts " myCount...#{zzarray.response.myCount}\n"
    i = 0
    zzarray.returndata.dcRec_t.each do |dcRec_t|
      i += 1
      puts "dcRec_t[#{i}]"
      puts "  Name...#{dcRec_t.dcMyName}"
      puts "  Job....#{dcRec_t.dcMyJob}"
      puts "  Rank...#{dcRec_t.dcMyRank}"
      puts "  Pay....#{dcRec_t.dcMyPay}"
    end
    # verify
    self.match_value(__method__,__LINE__,'myName'     ,zzwho    ,zzarray.response.myName)
    self.match_value(__method__,__LINE__,'myMax'      ,count    ,zzarray.response.myMax)
    self.match_value(__method__,__LINE__,'myCount'    ,count    ,zzarray.response.myCount)
    for i in 0..zzarray.returndata.dcRec_t.count-1
      dcMyName = zzarray.response.myName.to_s + (i+1).to_s
      dcMyJob = "Test " + (100 + i + 1).to_s
      dcMyRank = 10 + i + 1
      dcMyPay = 13.42 * (i+1)
      self.match_value(__method__,__LINE__,'dcMyName' ,dcMyName ,zzarray.returndata.dcRec_t[i].dcMyName)
      self.match_value(__method__,__LINE__,'dcMyJob'  ,dcMyJob  ,zzarray.returndata.dcRec_t[i].dcMyJob)
      self.match_value(__method__,__LINE__,'dcMyRank' ,dcMyRank ,zzarray.returndata.dcRec_t[i].dcMyRank)
      self.match_value(__method__,__LINE__,'dcMyPay'  ,dcMyPay  ,zzarray.returndata.dcRec_t[i].dcMyPay)
    end

    # output
    puts "\n+++++++++ output long ++++++++++++++++"
    puts "#{zzarray.name}.#{zzarray.func}:\n"
    puts " myName....#{zzarray.response.PARM0.myName}\n"
    puts " myMax.....#{zzarray.response.PARM1.myMax}\n"
    puts " myCount...#{zzarray.response.PARM2.myCount}\n"
    i = 0
    zzarray.returndata.douaggr.dcRec_t.each do |dcRec_t|
      i += 1
      puts "dcRec_t[#{i}]"
      puts "  Name...#{dcRec_t.dcMyName}"
      puts "  Job....#{dcRec_t.dcMyJob}"
      puts "  Rank...#{dcRec_t.dcMyRank}"
      puts "  Pay....#{dcRec_t.dcMyPay}"
    end
    # verify
    self.match_value(__method__,__LINE__,'myName'     ,zzwho    ,zzarray.response.PARM0.myName)
    self.match_value(__method__,__LINE__,'myMax'      ,count    ,zzarray.response.PARM1.myMax)
    self.match_value(__method__,__LINE__,'myCount'    ,count    ,zzarray.response.PARM2.myCount)
    for i in 0..zzarray.returndata.douaggr.dcRec_t.count-1
      dcMyName = zzarray.response.PARM0.myName.to_s + (i+1).to_s
      dcMyJob = "Test " + (100 + i + 1).to_s
      dcMyRank = 10 + i + 1
      dcMyPay = 13.42 * (i+1)
      self.match_value(__method__,__LINE__,'dcMyName' ,dcMyName ,zzarray.returndata.douaggr.dcRec_t[i].dcMyName)
      self.match_value(__method__,__LINE__,'dcMyJob'  ,dcMyJob  ,zzarray.returndata.douaggr.dcRec_t[i].dcMyJob)
      self.match_value(__method__,__LINE__,'dcMyRank' ,dcMyRank ,zzarray.returndata.douaggr.dcRec_t[i].dcMyRank)
      self.match_value(__method__,__LINE__,'dcMyPay'  ,dcMyPay  ,zzarray.returndata.douaggr.dcRec_t[i].dcMyPay)
    end
  end

end
