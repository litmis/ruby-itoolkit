# File: test_80010_DSLCallPgmZZCALL.rb
require "../test_authorization/auth"
require "../test_data/rowcol"

#   D  INCHARA        S              1a
#   D  INCHARB        S              1a
#   D  INDEC1         S              7p 4        
#   D  INDEC2         S             12p 2
#   D  INDS1          DS                  
#   D   DSCHARA                      1a
#   D   DSCHARB                      1a           
#   D   DSDEC1                       7p 4      
#   D   DSDEC2                      12p 2            
#    *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#    * main(): Control flow
#    *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#   C     *Entry        PLIST                   
#   C                   PARM                    INCHARA
#   C                   PARM                    INCHARB
#   C                   PARM                    INDEC1
#   C                   PARM                    INDEC2
#   C                   PARM                    INDS1

class Test_80010_DSLCallPgmZZCALL < RowColUnitTest
  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------
  def test_0030_pgm_zzcall_array
    # call IBM i ZZCALL (default values)
    zzcall = XMLService::I_PGM.new("ZZCALL",$toolkit_test_lib,{'error'=>'fast'})
    zzcall.input_parms do
     char "inchara", 1
     char "incharb", 1
     dec "indec1", 7, 4
     dec "indec2", 12, 2
     struct("inds1", 1) do
       char "dschara", 1
       char "dscharb", 1
       dec "dsdec1", 7, 4
       dec "dsdec2", 12, 2
     end
    end

    # set it Tony way ... PARMn is implied 
    zzcall.input.PARM0.inchara          = 'a'
    zzcall.input.PARM1.incharb          = 'a'
    zzcall.input.PARM2.indec1           = 11.1111
    zzcall.input.PARM3.indec2           = 11.11
    zzcall.input.PARM4.inds1[0].dschara = 'a'
    zzcall.input.PARM4.inds1[1].dscharb = 'a'
    zzcall.input.PARM4.inds1[2].dsdec1  = 111.1111
    zzcall.input.PARM4.inds1[3].dsdec2  = 1111.11
    # puts zzcall.inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts zzcall.dump_inspect
    puts zzcall.to_xml

    self.match_value(__method__,__LINE__,'inchara' ,'a'        ,zzcall.input.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'a'        ,zzcall.input.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,11.1111    ,zzcall.input.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,11.11      ,zzcall.input.PARM3.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'a'        ,zzcall.input.PARM4.inds1[0].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'a'        ,zzcall.input.PARM4.inds1[1].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,111.1111   ,zzcall.input.PARM4.inds1[2].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,1111.11    ,zzcall.input.PARM4.inds1[3].dsdec2)

    # set it Don way ... what is a parm?
    zzcall.input.inchara          = 'b'
    zzcall.input.incharb          = 'b'
    zzcall.input.indec1           = 22.2222
    zzcall.input.indec2           = 22.22
    zzcall.input.inds1[0].dschara = 'b'
    zzcall.input.inds1[1].dscharb = 'b'
    zzcall.input.inds1[2].dsdec1  = 222.2222
    zzcall.input.inds1[3].dsdec2  = 2222.22
    # puts zzcall.inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts zzcall.dump_inspect
    puts zzcall.to_xml

    self.match_value(__method__,__LINE__,'inchara' ,'b'        ,zzcall.input.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'b'        ,zzcall.input.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,22.2222    ,zzcall.input.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,22.22     ,zzcall.input.PARM3.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'b'        ,zzcall.input.PARM4.inds1[0].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'b'        ,zzcall.input.PARM4.inds1[1].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,222.2222   ,zzcall.input.PARM4.inds1[2].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,2222.22    ,zzcall.input.PARM4.inds1[3].dsdec2)


    # call IBM i ZZCALL
    zzcall.execute

    #puts zzcall.inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts zzcall.dump_inspect

    puts "\n+++++++++ output xml ++++++++++++++++"
    puts zzcall.out_xml


    # set it Aaron way ... just pass the data 
    parms = [ {'inchara'=>'1'},
              {'incharb'=>'1'},
              {'indec1'=>11.1111},
              {'indec2'=>111.11},
              {'inds1'=>[{'dschara'=>'2'},{'dscharb'=>'2'},{'dsdec1'=>22.2222},{'dsdec2'=>222.22}]} ]
    zzcall.call(parms)

    puts "\n+++++++++ input xml ++++++++++++++++"
    puts zzcall.to_xml

    self.match_value(__method__,__LINE__,'inchara' ,'1'        ,zzcall.input.PARM0.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'1'        ,zzcall.input.PARM1.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,11.1111    ,zzcall.input.PARM2.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,111.11     ,zzcall.input.PARM3.indec2)
    self.match_value(__method__,__LINE__,'dschara' ,'2'        ,zzcall.input.PARM4.inds1[0].dschara)
    self.match_value(__method__,__LINE__,'dscharb' ,'2'        ,zzcall.input.PARM4.inds1[1].dscharb)
    self.match_value(__method__,__LINE__,'dsdec1'  ,22.2222   ,zzcall.input.PARM4.inds1[2].dsdec1)
    self.match_value(__method__,__LINE__,'dsdec2'  ,222.22    ,zzcall.input.PARM4.inds1[3].dsdec2)

    #puts zzcall.inspect
    puts "\n+++++++++ dump_inspect ++++++++++++++++"
    puts zzcall.dump_inspect

    puts "\n+++++++++ output xml ++++++++++++++++"
    puts zzcall.out_xml

    # response xml with fewer dots ... clever naming
    puts "\n+++++++++ output fewer ++++++++++++++++"
    puts " inchara...#{zzcall.response.inchara}\n"
    puts " incharb...#{zzcall.response.incharb}\n"
    puts " indec1....#{zzcall.response.indec1}\n"
    puts " indec2....#{zzcall.response.indec2}\n"
    zzcall.response.inds1.each do |inds1|
      puts "  dschara...#{inds1.dschara}"
      puts "  dscharb...#{inds1.dscharb}"
      puts "  dsdec1....#{inds1.dsdec1}"
      puts "  dsdec2....#{inds1.dsdec2}"
    end
    # verify output
    self.match_value(__method__,__LINE__,'inchara' ,'C'           ,zzcall.response.inchara)
    self.match_value(__method__,__LINE__,'incharb' ,'D'           ,zzcall.response.incharb)
    self.match_value(__method__,__LINE__,'indec1'  ,321.1234      ,zzcall.response.indec1)
    self.match_value(__method__,__LINE__,'indec2'  ,1234567890.12 ,zzcall.response.indec2)
    zzcall.response.inds1.each do |inds1|
      self.match_value(__method__,__LINE__,'dschara' ,'E'           ,inds1.dschara)
      self.match_value(__method__,__LINE__,'dscharb' ,'F'           ,inds1.dscharb)
      self.match_value(__method__,__LINE__,'dsdec1'  ,333.333       ,inds1.dsdec1)
      self.match_value(__method__,__LINE__,'dsdec2'  ,4444444444.44 ,inds1.dsdec2)
    end


  end

end

