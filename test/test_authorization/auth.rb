require 'xmlservice'

# =================
# IBM i tests (see xmlservice/README_IBM_i)
# =================
$test_url        = ENV['TEST_URL']
$test_db         = ENV['TEST_DATABASE']
$test_user       = ENV['TEST_USER']
$test_pwd        = ENV['TEST_PWD']
$test_ctl        = ENV['TEST_CTL']
$test_ipc        = ENV['TEST_IPC']
$test_xmlservice = ENV['TEST_XMLSERVICE']
$test_lib        = ENV['TEST_LIB']
if $test_url == nil
  $test_url = "http://174.79.32.155/cgi-bin/xmlcgi.pgm"
end
if $test_db == nil
  $test_db = "*LOCAL"
end
if $test_user == nil
  $test_user = "*NONE"
end
if $test_pwd == nil
  $test_pwd = "*NONE"
end
if $test_ctl == nil
  $test_ctl = "*here"
end
if $test_ipc == nil
  $test_ipc = "*na"
end
if $test_xmlservice == nil
  $test_xmlservice = "XMLSERVICE"
end
if $test_lib == nil
  $test_lib = "DB2"
end

# =================
# developer only yaml 
# encrypt password tests
# =================
# $test_url = "yaml"
$test_env = ENV['TEST_ENV']
$test_yaml = ENV['TEST_YAML']
if $test_yaml == nil
  $test_yaml = "../test_authorization/xmlservice.yml"
end
if $test_env == nil
  $test_env = "db2public"
end


case $test_url
when "ibm_db"
  # =================
  # establish connection (DB2)
  # =================
  ActiveXMLService::Base.establish_connection(
  :adapter => "ibm_db",
  :connection => "ActiveRecord",
  :database => $test_db,
  :username => $test_user,
  :password  => $test_pwd,
  :ctl => $test_ctl,
  :ipc => $test_ipc,
  :size => 15000000,
  :head => "<?xml version='1.0'?>",
  :test_i_lib => $test_xmlservice,  # custom attributes (not xmlservice required)
  :test_i_db2_lib => $test_lib      # custom attributes (not xmlservice required)
  )
when "yaml"
  # =================
  # establish connection (based on yaml)
  # =================
  ActiveXMLService::Base.establish_connection(
  :test_yaml => $test_yaml,
  :test_env  => $test_env
  )
else
  # =================
  # establish connection (REST)
  # =================
  ActiveXMLService::Base.establish_connection(
  :connection => $test_url,
  :database => $test_db,
  :username => $test_user,
  :password  => $test_pwd,
  :ctl => $test_ctl,
  :ipc => $test_ipc,
  :size => 15000000,
  :head => "<?xml version='1.0'?>",
  :test_i_lib => $test_xmlservice,  # custom attributes (not xmlservice required)
  :test_i_db2_lib => $test_lib      # custom attributes (not xmlservice required)
  )
end

# custom attributes (not xmlservice required)
$toolkit_test_lib = ActiveXMLService::Base.configurations[:test_i_lib];
$toolkit_test_db2_lib = ActiveXMLService::Base.configurations[:test_i_db2_lib];

