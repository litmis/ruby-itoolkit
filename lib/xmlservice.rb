require 'adapters/abstract_adapter'
require 'yaml'
require 'thread'

# see README
module ActiveXMLService
 XINSTALL = "POWER_RUBY"
 XCTL = "*here *cdata"
 XIPC = "*none"
 XSIZE = 15000000
 XHEAD = "<?xml version='1.0'?>"
 XDATABASE = "*LOCAL"
 XUSERNAME = "*NONE"
 XPASSWORD = "*NONE"
 class Base
  # Converts all +config+ keys to symbols
  def self.symbolize_keys(config)
    # config = config.symbolize_keys
    if config
      config.keys.each do |key|
        config[(key.to_sym rescue key) || key] = config.delete(key)
      end
    end
    config
  end
  # configuration helper (singleton)
  def self.establish_configuration(config)
    # Converts all +config+ keys to symbols
    config = ActiveXMLService::Base.symbolize_keys(config)
    # *NEW test by yaml (see ibm_db_password.rb)
    if ENV['TEST_YAML']
      config[:test_yaml] = ENV['TEST_YAML'] 
    end
    if ENV['TEST_ENV']
      config[:test_env] = ENV['TEST_ENV'] 
    end
    if config.has_key?(:test_yaml) &&  config.has_key?(:test_env) 
      require 'password/password'
      config = XMLSERVICEPassword::Encrypt.parse_yaml(config,config[:test_yaml],config[:test_env])
      config = XMLSERVICEPassword::Encrypt.symbolize_keys(config)
    end
    # *NEW encrypted passwords - decrypt password or password.yml (see ibm_db_password.rb)
    if config.has_key?(:pwd_yaml) || config.has_key?(:pwd_enc)
      require 'password/password'
      config = XMLSERVICEPassword::Encrypt.parse_user_config(config)
    end
    @xml_config = config
    @xml_config
  end
  # last configuration (singleton)
  def self.configurations
    @xml_config
  end
  # adapter factory (singleton)
  def self.adapter_factory(config)
    config = ActiveXMLService::Base.establish_configuration(config)
    adapter = XMLServiceAdapters::adapter_factory(config)
    adapter
  end
  # standard rails connection (singleton)
  def self.establish_connection(config)
    @xml_adapter = ActiveXMLService::Base.adapter_factory(config)
  end
  def self.connection
    @xml_adapter
  end
  # *NEW encrypted passwords - generate pass key cipher (see ibm_db_password.rb)
  def self.generate_key()
    require 'password/password'
    key_out = XMLSERVICEPassword::Encrypt.gen_key()
    key_out
  end
  # *NEW encrypted passwords - generate encrypted password (see ibm_db_password.rb)
  # plaintext - password in plain text 
  # key       - 32 char pass key 
  def self.generate_password(plaintext, key="*DEFAULT")
    require 'password/password'
    pwd_out = XMLSERVICEPassword::Encrypt.gen_password(plaintext, key)
    pwd_out
  end
  # thread level one at a time
  def self.semaphore
    if !(defined? @xml_semaphore)
      @xml_semaphore = Mutex.new
    end
    @xml_semaphore
  end

 end # Base

end # module ActiveXMLService

module XMLService
 # ---------------------
 # BASE classes
 # ---------------------
 class I_Meta
  def add_user_accessor(name, value)
    if !(defined?(name)).nil?
      self.class.send(:attr_accessor, name)
    end
    instance_variable_set("@#{name}", value)
    name_var = "xml_user_accessor"
    if !(defined?(name_var)).nil?
      self.class.send(:attr_accessor, name_var)
    end
    instance_variable_set("@#{name_var}", name)
  end
  def remove_user_accessor(name)
    if self.instance_variable_defined?("@#{name}")
      self.remove_instance_variable("@#{name}")
    end
    name_var = "user_accessor"
    if self.instance_variable_defined?("@#{name_var}")
      self.remove_instance_variable("@#{name_var}")
    end
  end
  def instance_variable_forward_get(parent,pname,cname)
    if !parent.class.respond_to?("#{cname}")
      parent.class.instance_eval do
        define_method("#{cname}") { eval "@#{pname}.#{cname}" }
      end
    end
  end
  def instance_variable_forward_set(parent,pname,cname,cvalue)
    if !parent.class.respond_to?("#{cname}=")
      parent.class.instance_eval do
        define_method("#{cname}=") {|value| eval "@#{pname}.#{cname} = value" }
      end
    end
  end
  def shortCut(obj,parent)
    if parent.kind_of? I_Parameter or parent.kind_of? I_Return
      child = parent.value
      if child.kind_of? I_BaseMulti
        obj.add_user_accessor(child.var,child)
      elsif child.kind_of? I_Base
        obj.instance_variable_forward_get(obj,parent.var,child.var)
        obj.instance_variable_forward_set(obj,parent.var,child.var,child.value)
        obj.instance_variable_forward_get(parent,parent.var,child.var)
        obj.instance_variable_forward_set(parent,parent.var,child.var,child.value)
      elsif child.instance_of? Array
        child.each do |v|
          if v.kind_of? I_BaseMulti
            t = eval "obj.#{parent.var}.#{v.var}"
            obj.add_user_accessor(v.var,t)
          elsif parent.kind_of? I_Return and child.count == 1
            t = eval "obj.#{parent.var}.#{v.var}"
            obj.add_user_accessor(v.var,t)
          end
        end
      end
    end
  end
  def parse_output_attr()
  end
  def parse_return_attr()
  end
  def parse_diag_attr()
    diag = false
	  diag
  end
 end
 # ---------------------
 # data types
 # ---------------------
 class I_Base < I_Meta
  def initialize *args
    case args.size
    when 0
      initialize_zero *args
    when 1
      initialize_doc *args
    when 2
      initialize_type2 *args
    when 3
      initialize_type3 *args
    when 4
      initialize_type4 *args
    when 5
      initialize_type5 *args
    when 6
      initialize_type6 *args
    when 7
      initialize_type7 *args
    else
      raise
    end
  end
  def initialize_zero
    @xml_type = nil
    @xml_size = nil
    @xml_prec = nil
    @xml_vary = nil
    @xml_var  = nil
    @xml_data = nil
    @xml_enddo = nil
  end
  def initialize_doc(element)
    @xml_var  = element.attributes['var']
    @xml_vary = element.attributes['vary']
    @xml_enddo = element.attributes['enddo']
    type  = element.attributes['type']
    tp    = self.typeParse(type,@xml_var)
    @xml_type = tp['type']
    @xml_size = tp['size']
    @xml_prec = tp['prec']
    self.setValue(element.text)
    self.add_user_accessor(@xml_var,@xml_data)
  end
  def initialize_type2(v1,v2)
    raise "#{self.class.name}.new(#{v1},#{v2}) invalid number of parameters" 
  end
  def initialize_type3(v1,v2,v3)
    raise "#{self.class.name}.new(#{v1},#{v2},#{v3}) invalid number of parameters" 
  end
  def initialize_type4(v1,v2,v3,v4)
    raise "#{self.class.name}.new(#{v1},#{v2},#{v3},#{v4}) invalid number of parameters" 
  end
  def initialize_type5(v1,v2,v3,v4,v5)
    raise "#{self.class.name}.new(#{v1},#{v2},#{v3},#{v4},#{v5}) invalid number of parameters" 
  end
  def initialize_type6(v1,v2,v3,v4,v5,v6)
    raise "#{self.class.name}.new(#{v1},#{v2},#{v3},#{v4},#{v5},#{v6}) invalid number of parameters" 
  end
  def initialize_type7(v1,v2,v3,v4,v5,v6,v7)
    self.initialize_value(v1,v2,v3,v4,v5,v6,v7,"(type=#{v1},size=#{v2},prec=#{v3},var=#{v4},varying=#{v5},data=#{v6},enddo=#{v6})")
  end
  def check_nil(v,msg,hint)
    if v == nil
      raise "#{self.class.name} #{hint} #{msg}"
    end
  end
  def check_number(v)
    answer = true
    begin
      if Float(v) != nil
        answer = true
      end
    rescue
        answer = false
    end
    answer
  end
  def check_var(v,hint)
    msg = "invalid var"
    check_nil(v,msg,hint)
    if v and ( v.instance_of?(String) || v.instance_of?(Symbol) )
    else
      raise "#{self.class.name} #{hint} #{msg}"
    end
  end
  def check_type(v,hint)
    msg = "invalid type"
    check_nil(v,msg,hint)
    case v
    when "a" 
    when "i"
    when "u" 
    when "f" 
    when "p" 
    when "s" 
    when "b" 
    else
      raise "#{self.class.name} #{hint} #{msg}"
    end
  end
  def check_size(v,hint)
    msg = "invalid size"
    check_nil(v,msg,hint)
    data = v.to_i
    if data < 1 or !check_number(v)
      raise "#{self.class.name} #{hint} #{msg}"
    end
  end
  def check_prec(v,hint)
    msg = "invalid prec"
    check_nil(v,msg,hint)
    data = v.to_i
    if data < 0 or !check_number(v)
      raise "#{self.class.name} #{hint} #{msg}"
    end
  end
  def check_vary(v,hint)
    msg = "invalid vary"
    check_nil(v,msg,hint)
    if v == false
    else
      data = v.to_i
      if data == 2 or data == 4
      else
        raise "#{self.class.name} #{hint} #{msg}"
      end
    end
  end
  def check_enddo(v,hint)
    msg = "invalid enddo"
    if v == nil
    else
      if v.instance_of? String
      else
        raise "#{self.class.name} #{hint} #{msg}"
      end
    end
  end
  def initialize_value(type,size,prec,var,vary,data,enddo,hint)
    check_type(type,hint)
    check_size(size,hint)
    check_prec(prec,hint)
    check_var(var,hint)
    check_vary(vary,hint)
    check_enddo(enddo,hint)
    @xml_type = type
    @xml_size = size 
    @xml_prec = prec 
    @xml_vary = vary
    @xml_var  = var
    @xml_enddo = enddo
    self.add_user_accessor(var,data)
    self.setValue(data)
  end
  def type
    @xml_type
  end
  def size
    @xml_size
  end
  def precision
    @xml_prec
  end
  def vary
    @xml_vary
  end
  def var
    @xml_var
  end
  def dupToUserValue
    if defined? xml_user_accessor and !@xml_user_accessor.nil? and !@xml_user_accessor.empty?
      instance_variable_set("@#{xml_user_accessor}", @xml_data)
    end
  end
  def dupFromUserValue
    if defined? xml_user_accessor and !@xml_user_accessor.nil? and !@xml_user_accessor.empty?
      @xml_data = instance_variable_get("@#{xml_user_accessor}")
    end
  end
  def setValue(data)
    if data == nil
      rdata = 0
      case @xml_type
      when "a"
        rdata = ""
      when "b"
        rdata = "00"
      end
    elsif data.is_a?(XMLService::I_Base)
      rdata = data.value
    else
      rdata = data
    end
    case @xml_type
    when "a" 
      @xml_data = String.new(rdata)
    when "i"
      @xml_data = rdata.to_i 
    when "u" 
      @xml_data = rdata.to_i 
    when "f" 
      @xml_data = rdata.to_f
    when "p" 
      @xml_data = rdata.to_f
    when "s" 
      @xml_data = rdata.to_f
    when "b" 
      @xml_data = rdata
    end
    dupToUserValue
  end
  def value
    dupFromUserValue
    if @xml_data == nil
      @xml_data = 0
      case @xml_type
      when "a"
        @xml_data = ""
      when "b"
        @xml_data = "00"
      end
      dupToUserValue
    end
    case @xml_type
    when "a" 
      data = @xml_data.to_s
    when "i"
      data = @xml_data.to_i 
    when "u" 
      data = @xml_data.to_i 
    when "f" 
      data = @xml_data.to_f
    when "p" 
      data = @xml_data.to_f
    when "s" 
      data = @xml_data.to_f
    when "b" 
      data = @xml_data # .pack('H')
    else
      data = @xml_data
    end
    data
  end
  def to_s
    dupFromUserValue
    data = @xml_data.to_s
    data
  end
  def to_f
    dupFromUserValue
    data = @xml_data.to_f
    data
  end
  def to_i
    dupFromUserValue
    data = @xml_data.to_i
    data
  end
  def to_xml
    dupFromUserValue
    xml = ""
    data = @xml_data.to_s
    enddo = ""
    if @xml_enddo
      enddo = " enddo='#{@xml_enddo}'"
    end
    vary = ""
    if @xml_vary
      vary = " varying='#{@xml_vary}'"
    end
    case @xml_type
    when "a" 
      xml = "<data var='#{@xml_var}' type='#{@xml_size}#{@xml_type}'#{vary}>#{data}</data>\n"
    when "i"
      if @xml_size < 2
        type = " type='3#{@xml_type}0'"
      elsif @xml_size < 4
        type = " type='5#{@xml_type}0'"
      elsif @xml_size < 8
        type = " type='10#{@xml_type}0'"
      else
        type = " type='20#{@xml_type}0'"
      end
      xml = "<data var='#{@xml_var}'#{type}#{enddo}>#{data}</data>\n"
    when "u" 
      if @xml_size < 2
        type = " type='3#{@xml_type}0'"
      elsif @xml_size < 4
        type = " type='5#{@xml_type}0'"
      elsif @xml_size < 8
        type = " type='10#{@xml_type}0'"
      else
        xml = " type='20#{@xml_type}0'"
      end
      xml = "<data var='#{@xml_var}'#{type}#{enddo}>#{data}</data>\n"
    when "f" 
      xml = "<data var='#{@xml_var}' type='#{@xml_size}#{@xml_type}#{@xml_prec}#{enddo}'>#{data}</data>\n"
    when "p" 
      xml = "<data var='#{@xml_var}' type='#{@xml_size}#{@xml_type}#{@xml_prec}#{enddo}'>#{data}</data>\n"
    when "s" 
      xml = "<data var='#{@xml_var}' type='#{@xml_size}#{@xml_type}#{@xml_prec}#{enddo}'>#{data}</data>\n"
    when "b" 
      xml = "<data var='#{@xml_var}' type='#{@xml_size}#{@xml_type}'>#{data}</data>\n"
    end
    xml
  end
  def typeParse(type, vary)
    tp = Hash.new
    tp['type'] = nil
    tp['vary'] = 0
    tp['size'] = 0
    tp['prec'] = 0
    if type.index('a')
      size = type.split('a')
      tp['type'] = "a"
      tp['size'] = size[0]
      if vary == 2
        tp['vary'] = 2
      elsif vary == 4
        tp['vary'] = 4
      else
      end
    elsif type.index('i')
      size = type.split('i')
      tp['type'] = "i"
      tp['size'] = size[0]
    elsif type.index('u')
      size = type.split('u')
      tp['type'] = "u"
      tp['size'] = size[0]
    elsif type.index('f')
      size = type.split('f')
      tp['type'] = "f"
      tp['size'] = size[0]
    elsif type.index('p')
      size = type.split('p')
      tp['type'] = "p"
      tp['size'] = size[0]
      tp['prec'] = size[1]
    elsif type.index('s')
      size = type.split('s')
      tp['type'] = "s"
      tp['size'] = size[0]
      tp['prec'] = size[1]
    elsif type.index('b')
      size = type.split('b')
      tp['type'] = "b"
      tp['size'] = size[0]
    end
    tp['prec'] = tp['prec'].to_i
    tp['size'] = tp['size'].to_i
    tp
  end
 end
 # ---------------------
 # data types
 # ---------------------
 class I_a < I_Base
  def initialize_zero
    self.initialize_value("a",5,0,"char",false,"blank",nil,"()")
  end
  def initialize_type3(var,size,data)
    self.initialize_value("a",size,0,var,false,data,nil,"(var=#{var},size=#{size},data=#{data})")
  end
 end
 class I_String < I_a
 end
 class I_Char < I_a
 end
 class I_a_varying < I_Base
  def initialize_zero
    self.initialize_value("a",5,0,"vchar2",2,"blank",nil,"()")
  end
  def initialize_type3(var,size,data)
    self.initialize_value("a",size,0,var,2,data,nil,"(var=#{var},size=#{size},data=#{data})")
  end
 end
 class I_a_varying_2 < I_a_varying
 end
 class I_VarChar2 < I_a_varying
 end
 class I_a_varying_4 < I_Base
  def initialize_zero
    self.initialize_value("a",5,0,"vchar4",4,"blank",nil,"()")
  end
  def initialize_type3(var,size,data)
    self.initialize_value("a",size,0,var,4,data,nil,"(var=#{var},size=#{size},data=#{data})")
  end
 end
 class I_VarChar4 < I_a_varying_4
 end
 class I_3i0 < I_Base
  def initialize_zero
    self.initialize_value("i",1,0,"int8",false,0,nil,"()")
  end
  def initialize_type2(var,data)
    self.initialize_value("i",1,0,var,false,data,nil,"(var=#{var},data=#{data})")
  end
  def initialize_type3(var,data,enddo)
    self.initialize_value("i",1,0,var,false,data,enddo,"(var=#{var},data=#{data},enddo=#{enddo})")
  end
 end
 class I_Int8 < I_3i0
 end
 class I_5i0 < I_Base
  def initialize_zero
    self.initialize_value("i",2,0,"int16",false,0,nil,"()")
  end
  def initialize_type2(var,data)
    self.initialize_value("i",2,0,var,false,data,nil,"(var=#{var},data=#{data})")
  end
  def initialize_type3(var,data,enddo)
    self.initialize_value("i",2,0,var,false,data,enddo,"(var=#{var},data=#{data},enddo=#{enddo})")
  end
 end
 class I_Int16 < I_5i0
 end
 class I_10i0 < I_Base
  def initialize_zero
    self.initialize_value("i",4,0,"int32",false,0,nil,"()")
  end
  def initialize_type2(var,data)
    self.initialize_value("i",4,0,var,false,data,nil,"(var=#{var},data=#{data})")
  end
  def initialize_type3(var,data,enddo)
    self.initialize_value("i",4,0,var,false,data,enddo,"(var=#{var},data=#{data},enddo=#{enddo})")
  end
 end
 class I_Int32 < I_10i0
 end
 class I_20i0 < I_Base
  def initialize_zero
    self.initialize_value("i",8,0,"int64",false,0,nil,"()")
  end
  def initialize_type2(var,data)
    self.initialize_value("i",8,0,var,false,data,nil,"(var=#{var},data=#{data})")
  end
  def initialize_type3(var,data,enddo)
    self.initialize_value("i",8,0,var,false,data,enddo,"(var=#{var},data=#{data},enddo=#{enddo})")
  end
 end
 class I_Int64 < I_20i0
 end
 class I_3u0 < I_Base
  def initialize_zero
    self.initialize_value("u",1,0,"uint8",false,0,nil,"()")
  end
  def initialize_type2(var,data)
    self.initialize_value("u",1,0,var,false,data,nil,"(var=#{var},data=#{data})")
  end
  def initialize_type3(var,data,enddo)
    self.initialize_value("u",1,0,var,false,data,enddo,"(var=#{var},data=#{data},enddo=#{enddo})")
  end
 end
 class I_Uint8 < I_3u0
 end
 class I_5u0 < I_Base
  def initialize_zero
    self.initialize_value("u",2,0,"uint16",false,0,nil,"()")
  end
  def initialize_type2(var,data)
    self.initialize_value("u",2,0,var,false,data,nil,"(var=#{var},data=#{data})")
  end
  def initialize_type3(var,data,enddo)
    self.initialize_value("u",2,0,var,false,data,enddo,"(var=#{var},data=#{data},enddo=#{enddo})")
  end
 end
 class I_Uint16 < I_5u0
 end
 class I_10u0 < I_Base
  def initialize_zero
    self.initialize_value("u",4,0,"uint32",false,0,nil,"()")
  end
  def initialize_type2(var,data)
    self.initialize_value("u",4,0,var,false,data,nil,"(var=#{var},data=#{data})")
  end
  def initialize_type3(var,data,enddo)
    self.initialize_value("u",4,0,var,false,data,enddo,"(var=#{var},data=#{data},enddo=#{enddo})")
  end
 end
 class I_Uint32 < I_10u0
 end
 class I_20u0 < I_Base
  def initialize_zero
    self.initialize_value("u",8,0,"uint64",false,0,nil,"()")
  end
  def initialize_type2(var,data)
    self.initialize_value("u",8,0,var,false,data,nil,"(var=#{var},data=#{data})")
  end
  def initialize_type3(var,data,enddo)
    self.initialize_value("u",8,0,var,false,data,enddo,"(var=#{var},data=#{data},enddo=#{enddo})")
  end
 end
 class I_Uint64 < I_20u0
 end
 class I_4f < I_Base
  def initialize_zero
    self.initialize_value("f",4,2,"float",false,0,nil,"()")
  end
  def initialize_type3(var,prec,data)
    self.initialize_value("f",4,prec,var,false,data,nil,"(var=#{var},prec=#{prec},data=#{data})")
  end
  def initialize_type4(var,prec,data,enddo)
    self.initialize_value("f",4,prec,var,false,data,enddo,"(var=#{var},prec=#{prec},data=#{data},enddo=#{enddo})")
  end
 end
 class I_Float4 < I_4f
 end
 class I_Real < I_4f
 end
 class I_8f < I_Base
  def initialize_zero
    self.initialize_value("f",8,2,"double",false,0,nil,"()")
  end
  def initialize_type3(var,prec,data)
    self.initialize_value("f",8,prec,var,false,data,nil,"(var=#{var},prec=#{prec},data=#{data})")
  end
  def initialize_type4(var,prec,data,enddo)
    self.initialize_value("f",8,prec,var,false,data,enddo,"(var=#{var},prec=#{prec},data=#{data},enddo=#{enddo})")
  end
 end
 class I_Float8 < I_8f
 end
 class I_Double < I_8f
 end
 class I_p < I_Base
  def initialize_zero
    self.initialize_value("p",12,2,"packed",false,0,nil,"()")
  end
  def initialize_type4(var,size,prec,data)
    self.initialize_value("p",size,prec,var,false,data,nil,"(var=#{var},size=#{size},prec=#{prec},data=#{data})")
  end
  def initialize_type5(var,size,prec,data,enddo)
    self.initialize_value("p",size,prec,var,false,data,enddo,"(var=#{var},size=#{size},prec=#{prec},data=#{data},enddo=#{enddo})")
  end
 end
 class I_PackedDecimal < I_p
 end
 class I_z < I_Base
  def initialize_zero
    self.initialize_value("s",12,2,"zoned",false,0,nil,"()")
  end
  def initialize_type4(var,size,prec,data)
    self.initialize_value("s",size,prec,var,false,data,nil,"(var=#{var},size=#{size},prec=#{prec},data=#{data})")
  end
  def initialize_type5(var,size,prec,data,enddo)
    self.initialize_value("s",size,prec,var,false,data,enddo,"(var=#{var},size=#{size},prec=#{prec},data=#{data},enddo=#{enddo})")
  end
 end
 class I_ZonedDecimal < I_z
 end
 class I_b < I_Base
  def initialize_zero
    self.initialize_value("b",1,0,"hex",false,"00",nil,"()")
  end
  def initialize_type3(var,size,data)
    self.initialize_value("b",size,0,var,false,data.to_s(16).upcase,nil,"(var=#{var},size=#{size},data=#{data})")
  end
 end
 class I_Binary < I_b
 end
 # ---------------------
 # collections
 # ---------------------
 class I_BaseMulti < I_Meta
  def initialize *args
    case args.size
    when 0
      initialize_zero *args
    when 1
      initialize_doc *args
    when 2
      initialize_type2 *args
    when 3
      initialize_type3 *args
    when 4
      initialize_type4 *args
    else
      raise
    end
  end
  def initialize_zero
    @xml_var  = 'multi'
    @xml_elem = nil
  end
  def initialize_doc(element)
    @xml_var  = element.attributes['var']
    @xml_elem = self.elemParse(element)
  end
  def initialize_type2(v1,v2)
    raise "#{self.class.name}.new(#{v1},#{v2}) invalid number of parameters" 
  end
  def initialize_type3(v1,v2,v3)
    raise "#{self.class.name}.new(#{v1},#{v2},#{v3}) invalid number of parameters" 
  end
  def initialize_type4(v1,v2,v3,v4)
    raise "#{self.class.name}.new(#{v1},#{v2},#{v3},#{v4}) invalid number of parameters" 
  end
  def initialize_value(var,elem)
    @xml_var = var
    @xml_elem = elem
    self.add_user_accessor(var,elem)
  end
  def getIndexValue(idx)
    elem = nil
    if @xml_elem.instance_of? Array
      if idx.instance_of? String
        @xml_elem.each do | value |
          if value.var == idx
            elem = value
            break
          end
        end
      else
        elem = @xml_elem[idx]
      end
    elsif @xml_elem.instance_of? Hash
      if idx.instance_of? String
        @xml_elem.each do | key, value |
          if key == idx
            elem = value
            break
          end
        end
      end
    else
      if idx.instance_of? String
        if @xml_elem.var == idx
          elem = @xml_elem
        end
      else
        elem = @xml_elem
      end
    end
    elem
  end
  def var(idx=-1)
    var = "*NONE"
    if idx == -1
      var = @xml_var
    else
      elem = self.getIndexValue(idx)
      if elem
        var = elem.var
      end
    end
    var
  end
  def value(idx=-1)
    elem = nil
    if idx == -1
      elem = @xml_elem
    else
      elem = self.getIndexValue(idx)
    end
    elem
  end
  def assoc(idx)
    self.value(idx)
  end
  def [](idx)
    self.value(idx)
  end
  def count
    cnt = @xml_elem.count
	  cnt
  end
  def elemParse(doc)
    multi = Hash.new
    elems = Array.new
    base = I_Base.new
    doc.elements.each do |element|
      case element.name
      when "error"
		  if element.elements["errnoxml"] == nil
        add_user_accessor("fail", element.text)
		  else
        elem_add = I_Error.new(element)
        if !multi.has_key?(elem_add.var)
          multi[elem_add.var] = Array.new
        end
        count = multi[elem_add.var].count
        multi[elem_add.var][count] = elem_add
		  end
      when "success"
        add_user_accessor(element.name, element.text)
      when "version"
        add_user_accessor(element.name, element.text)
      when "jobinfo"
        elems[elems.count] = I_JobInfo.new(element)
        elem_add = elems[elems.count - 1]
        add_user_accessor(element.name, elem_add)
      when "joblogrec"
        elem_add = I_JobLogRec.new(element)
        if !multi.has_key?(elem_add.var)
          multi[elem_add.var] = Array.new
        end
        count = multi[elem_add.var].count
        multi[elem_add.var][count] = elem_add
      when "joblogscan"
        elems[elems.count] = I_JobLogScan.new(element)
        elem_add = elems[elems.count - 1]
        add_user_accessor(element.name, elem_add)
      when "joblog"
        elems[elems.count] = I_JobLog.new(element)
        elem_add = elems[elems.count - 1]
        add_user_accessor(element.name, elem_add)
      when "ds"
        elems[elems.count] = I_DS.new(element)
        elem_add = elems[elems.count - 1]
        add_user_accessor(elem_add.var, elem_add)
        if !multi.has_key?(elem_add.var)
          multi[elem_add.var] = Array.new
        end
        count = multi[elem_add.var].count
        multi[elem_add.var][count] = elem_add
      when "data"
        type = element.attributes['type']
        vary = element.attributes['varying']
        tp = base.typeParse(type, vary)
        case tp['type']
        when "a" 
          case tp['vary']
          when 0
            elems[elems.count] = I_Char.new(element)
          when 2 
            elems[elems.count] = I_VarChar2.new(element)
          when 4 
            elems[elems.count] = I_VarChar4.new(element)
          end
        when "i" 
          case tp['size']
          when 3
            elems[elems.count] = I_Int8.new(element)
          when 5
            elems[elems.count] = I_Int16.new(element)
          when 10
            elems[elems.count] = I_Int32.new(element)
          when 20
            elems[elems.count] = I_Int64.new(element)
          end
        when "u" 
          case tp['size']
          when 3
            elems[elems.count] = I_Uint8.new(element)
          when 5
            elems[elems.count] = I_Uint16.new(element)
          when 10
            elems[elems.count] = I_Uint32.new(element)
          when 20
            elems[elems.count] = I_Uint64.new(element)
          end
        when "f" 
          case tp['size']
          when 4
            elems[elems.count] = I_Float4.new(element)
          when 8 
            elems[elems.count] = I_Float8.new(element)
          end
        when "p" 
          elems[elems.count] = I_PackedDecimal.new(element)
        when "s" 
          elems[elems.count] = I_ZonedDecimal.new(element)
        when "b" 
          elems[elems.count] = I_Binary.new(element)
        end
        elem_add = elems[elems.count - 1]
        add_user_accessor(elem_add.var, elem_add)
      end
    end
    # fix-up multi (dim=1..n)
    multi.each do |var,elem|
      add_user_accessor(var, elem)
    end
    elems
  end
  def add(var, elem)
    @xml_elem[@xml_elem.count] = elem
  end
  def struct(var,dim,&block)
     self.add(var,I_DS.new(var,dim).input_elems(&block))
  end  
  def bin(var, size, data="00")
   self.add(var,I_b.new(var,size,data))
  end
  def char(var, size, data=" ")
   self.add(var,I_a.new(var,size,data))
  end
  def varchar(var, size, data=" ")
   self.add(var,I_a_varying.new(var,size,data))
  end
  def varchar4(var, size, data=" ")
   self.add(var,I_a_varying_4.new(var,size,data))
  end
  def tiny(var, data=0)
   self.add(var,I_Int8.new(var,data))
  end
  def short(var, data=0)
   self.add(var,I_Int16.new(var,data))
  end
  def long(var, data=0)
   self.add(var,I_Int32.new(var,data))
  end
  def longlong(var, data=0)
   self.add(var,I_Int64.new(var,data))
  end
  def utiny(var, data=0)
   self.add(var,I_Uint8.new(var,data))
  end
  def ushort(var, data=0)
   self.add(var,I_Uint16.new(var,data))
  end
  def ulong(var, data=0)
   self.add(var,I_Uint32.new(var,data))
  end
  def ulonglong(var, data=0)
   self.add(var,I_Uint64.new(var,data))
  end
  def real(var, scale, data=0.0)
   self.add(var,I_Float4.new(var,scale,data))
  end
  def float4(var, scale, data=0.0)
   self.add(var,I_Float4.new(var,scale,data))
  end
  def double(var, scale, data=0.0)
   self.add(var,I_Float8.new(var,scale,data))
  end
  def float8(var, scale, data=0.0)
   self.add(var,I_Float8.new(var,scale,data))
  end
  def dec(var, size, scale, data=0)
   self.add(var,I_p.new(var,size,scale,data))
  end
  def zone(var, size, scale, data=0)
   self.add(var,I_s.new(var,size,scale,data))
  end
  def input_elems(&block)
     self.instance_eval(&block)
     self
  end  
 end
  # ---------------------
 # diag (error)
 # ---------------------
 class I_DiagBase < I_Meta
  def initialize *args
    case args.size
    when 0
      initialize_zero *args
    when 1
      initialize_doc *args
    else
      raise
    end
  end
  def initialize_zero
    @xml_var = 'base'
  end
  def initialize_doc(element)
    if self.var == nil
      @xml_var = 'base'
    end
    elemParse(element)
  end
  def var
    @xml_var
  end
  def elemParse(doc)
    doc.elements.each do |element|
      add_user_accessor(element.name, element.text)
    end
  end
 end
 class I_Error < I_DiagBase
  def initialize_doc(element)
    @xml_var = 'error'
    super(element)
  end
  def elemParse(doc)
    doc.elements.each do |element|
      add_user_accessor(element.name, element.text.gsub(/\n/, ""))
    end
  end
 end
 class I_JobInfo < I_DiagBase
  def initialize_doc(element)
    @xml_var = 'jobinfo'
    super(element)
  end
 end
 class I_JobLogRec < I_DiagBase
  def initialize_doc(element)
    @xml_var = 'joblogrec'
    super(element)
  end
 end
 class I_Diag  < I_BaseMulti
  def initialize_zero
    @xml_var  = 'diag'
    @xml_elem = nil
  end
  def initialize_doc(element)
    @xml_var = 'diag'
    super(element)
  end
 end
 class I_JobLogScan < I_BaseMulti
  def initialize_zero
    @xml_var  = 'joblogscan'
    @xml_elem = nil
  end
  def initialize_doc(element)
    @xml_var = 'joblogscan'
    super(element)
  end
 end
 class I_JobLog < I_DiagBase
  def initialize_doc(element)
    @xml_var = 'joblog'
    super(element)
  end
  def elemParse(doc)
    joblog = ""
    doc.texts.each do |t|
      joblog << t.value
    end
    add_user_accessor("joblogdata", joblog)
  end
 end
# ---------------------
 # data structure collection
 # ---------------------
 class I_DS < I_BaseMulti
  def initialize_zero
    @xml_var  = 'ds'
    @xml_elem = nil
    self.initialize_value(var,elem)
  end
  def initialize_doc(element)
    @xml_dim = element.attributes['dim']
    @xml_dou = element.attributes['dou']
    super(element)
  end
  # for dsl
  def initialize_type2(var,dim)
    @xml_dim = 1
    if dim > 1
      @xml_dim = dim
    end
    @xml_dou = nil
    self.initialize_value(var,Array.new)
  end
  def initialize_type3(var,dim,elem)
    @xml_dim = 1
    if dim > 1
      @xml_dim = dim
    end
    # for dsl
    if elem.instance_of? String
      @xml_dou = elem
      self.initialize_value(var,Array.new)
    # normal ctor
    else
      @xml_dou = nil
      self.initialize_value(var,elem)
    end
  end
  def initialize_type4(var,dim,dou,elem)
    @xml_dim = 1
    if dim > 1
      @xml_dim = dim
    end
    @xml_dou = dou
    self.initialize_value(var,elem)
  end
  def to_xml_recursive(elem)
    xml = ""
    if elem.instance_of? Hash
      elem.each do |k,a|
        if a.instance_of? Hash or a.instance_of? Array
          xml << self.to_xml_recursive(a)
        else
          xml << a.to_xml
        end
      end
    elsif elem.instance_of? Array
      elem.each do |a|
        if a.instance_of? Hash or a.instance_of? Array
          xml << self.to_xml_recursive(a)
        else
          xml << a.to_xml
        end
      end
    end
    xml
  end
  def to_xml
    dim = " dim='#{@xml_dim}'"
    dou = ""
    if @xml_dou
      dou = " dou='#{@xml_dou}'"
    end
    xml = "<ds var='#{@xml_var}'#{dim}#{dou}>\n"
    xml << self.to_xml_recursive(@xml_elem)
    xml << "</ds>\n"
    xml
  end
 end
 # ---------------------
 # parameter
 # ---------------------
 class I_Parameter < I_BaseMulti
  def initialize_zero
    @xml_var  = 'parm'
    @xml_elem = nil
    self.initialize_value(var,elem)
  end
  def initialize_doc(element)
    @xml_io = element.attributes['io']
    super(element)
  end
  def initialize_type3(var,io,elem)
    @xml_io = io
    self.initialize_value(var,elem)
  end
  def to_xml
    xml = "<parm var='#{@xml_var}' io='#{@xml_io}'>\n"
    xml << @xml_elem.to_xml
    xml << "</parm>\n"
    xml
  end
 end
 # ---------------------
 # return
 # ---------------------
 class I_Return < I_BaseMulti
  def initialize_zero
    @xml_var  = 'ret'
    @xml_elem = nil
    self.initialize_value(var,elem)
  end
  def initialize_type2(var,elem)
    self.initialize_value(var,elem)
  end
  def to_xml
    xml = "<return var='#{@xml_var}'>\n"
    xml << @xml_elem.to_xml
    xml << "</return>\n"
    xml
  end
 end
 # ---------------------
 # xmlservice call
 # ---------------------
 class I_Data < I_Meta
 end
 class I_CALL < I_Meta
  attr_accessor :input, :reponse, :return
  def initialize(options)
    @xml_options = ActiveXMLService::Base.symbolize_keys(options)
	  @xml_is_error = true
    @input = I_Data.new
    @response = I_Data.new
    @returndata = I_Data.new
  end
  def xmlservice()
	  @xml_is_error = true
    # one thread at a time please
    ActiveXMLService::Base.semaphore.synchronize {
      # user supplied connection
      if @xml_options && @xml_options.has_key?(:connection)
        adapter = ActiveXMLService::Base.adapter_factory(@xml_options)
      # standard rails connection (singleton)
      else
        adapter = ActiveXMLService::Base.connection
        # I believe "the Rails way" would be to lead them down a best-practice path,
        # xmlservice over db2 stored procedures is most common connection (best)
        if adapter.nil?
          adapter = ActiveXMLService::Base.adapter_factory('connection'=>'ActiveRecord')
        end
      end
      adapter.xmlservice(self)
      @xml_doc = adapter.out_doc
    }
    @xml_is_error = self.parse_diag_attr
    self.parse_output_attr
    self.parse_return_attr
  end
  def call_recursive(k,v)
    if k.nil?
      k = "input"
    end
    if !v.nil?
      if v.instance_of? Hash
        v.each do | k1, v1 |
          if k.eql? "input"
#puts "Hash #{k}.#{k1}, #{v1}"
            call_recursive(k + "." + k1,v1)
          elsif v1.instance_of? Hash or v.instance_of? Array
#puts "Hash #{k}['#{k1}'], #{v1}"
            call_recursive(k + "['#{k1}']",v1)
          else
#puts "Hash #{k}.#{k1}, #{v1}"
            call_recursive(k + "." + k1,v1)
          end
        end
      elsif v.instance_of? Array
        for i in 0..v.count-1
          if k.eql? "input"
#puts "Array #{k}, #{v[1]}"
            call_recursive(k,v[i])
          else
#puts "Array #{k}[#{i}], #{v[1]}"
            call_recursive(k + "[#{i}]",v[i])
          end
        end
      elsif !k.eql? "input"
#puts "self.#{k} = #{v}"
        eval "self.#{k} = v"
      end
    end
  end
  def call(p0=nil,p1=nil,p2=nil,p3=nil,p4=nil,p5=nil,p6=nil,p7=nil,p8=nil,p9=nil,
           p10=nil,p11=nil,p12=nil,p13=nil,p14=nil,p15=nil,p16=nil,p17=nil,p18=nil,p19=nil,
           p20=nil,p21=nil,p22=nil,p23=nil,p24=nil,p25=nil,p26=nil,p27=nil,p28=nil,p29=nil,
           p30=nil,p31=nil,p32=nil,p33=nil,p34=nil,p35=nil,p36=nil,p37=nil,p38=nil,p39=nil,
           p40=nil,p41=nil,p42=nil,p43=nil,p44=nil,p45=nil,p46=nil,p47=nil,p48=nil,p49=nil,
           p50=nil,p51=nil,p52=nil,p53=nil,p54=nil,p55=nil,p56=nil,p57=nil,p58=nil,p59=nil,
           p60=nil,p61=nil,p62=nil,p63=nil,p64=nil)
    self.call_recursive(nil,p0)
    self.call_recursive(nil,p1)
    self.call_recursive(nil,p2)
    self.call_recursive(nil,p3)
    self.call_recursive(nil,p4)
    self.call_recursive(nil,p5)
    self.call_recursive(nil,p6)
    self.call_recursive(nil,p7)
    self.call_recursive(nil,p8)
    self.call_recursive(nil,p9)

    self.call_recursive(nil,p10)
    self.call_recursive(nil,p11)
    self.call_recursive(nil,p12)
    self.call_recursive(nil,p13)
    self.call_recursive(nil,p14)
    self.call_recursive(nil,p15)
    self.call_recursive(nil,p16)
    self.call_recursive(nil,p17)
    self.call_recursive(nil,p18)
    self.call_recursive(nil,p19)

    self.call_recursive(nil,p20)
    self.call_recursive(nil,p21)
    self.call_recursive(nil,p22)
    self.call_recursive(nil,p23)
    self.call_recursive(nil,p24)
    self.call_recursive(nil,p25)
    self.call_recursive(nil,p26)
    self.call_recursive(nil,p27)
    self.call_recursive(nil,p28)
    self.call_recursive(nil,p29)

    self.call_recursive(nil,p30)
    self.call_recursive(nil,p31)
    self.call_recursive(nil,p32)
    self.call_recursive(nil,p33)
    self.call_recursive(nil,p34)
    self.call_recursive(nil,p35)
    self.call_recursive(nil,p36)
    self.call_recursive(nil,p37)
    self.call_recursive(nil,p38)
    self.call_recursive(nil,p39)

    self.call_recursive(nil,p40)
    self.call_recursive(nil,p41)
    self.call_recursive(nil,p42)
    self.call_recursive(nil,p43)
    self.call_recursive(nil,p44)
    self.call_recursive(nil,p45)
    self.call_recursive(nil,p46)
    self.call_recursive(nil,p47)
    self.call_recursive(nil,p48)
    self.call_recursive(nil,p49)

    self.call_recursive(nil,p50)
    self.call_recursive(nil,p51)
    self.call_recursive(nil,p52)
    self.call_recursive(nil,p53)
    self.call_recursive(nil,p54)
    self.call_recursive(nil,p55)
    self.call_recursive(nil,p56)
    self.call_recursive(nil,p57)
    self.call_recursive(nil,p58)
    self.call_recursive(nil,p59)

    self.call_recursive(nil,p60)
    self.call_recursive(nil,p61)
    self.call_recursive(nil,p62)
    self.call_recursive(nil,p63)
    self.call_recursive(nil,p64)

    self.xmlservice
  end
  def execute(p0=nil,p1=nil,p2=nil,p3=nil,p4=nil,p5=nil,p6=nil,p7=nil,p8=nil,p9=nil,
           p10=nil,p11=nil,p12=nil,p13=nil,p14=nil,p15=nil,p16=nil,p17=nil,p18=nil,p19=nil,
           p20=nil,p21=nil,p22=nil,p23=nil,p24=nil,p25=nil,p26=nil,p27=nil,p28=nil,p29=nil,
           p30=nil,p31=nil,p32=nil,p33=nil,p34=nil,p35=nil,p36=nil,p37=nil,p38=nil,p39=nil,
           p40=nil,p41=nil,p42=nil,p43=nil,p44=nil,p45=nil,p46=nil,p47=nil,p48=nil,p49=nil,
           p50=nil,p51=nil,p52=nil,p53=nil,p54=nil,p55=nil,p56=nil,p57=nil,p58=nil,p59=nil,
           p60=nil,p61=nil,p62=nil,p63=nil,p64=nil)
    self.call(p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,
              p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,
              p20,p21,p22,p23,p24,p25,p26,p27,p28,p29,
              p30,p31,p32,p33,p34,p35,p36,p37,p38,p39,
              p40,p41,p42,p43,p44,p45,p46,p47,p48,p49,
              p50,p51,p52,p53,p54,p55,p56,p57,p58,p59,
              p60,p61,p62,p63,p64)
  end
  def xmlservice_error
	  @xml_is_error
  end
  def input()
    @input
  end
  def response()
    @response
  end
  def parse_output_attr()
    @response = I_Data.new
  end
  def returndata()
    @returndata
  end
  def parse_return_attr()
    @returndata = I_Data.new
  end
  def xmlservice_diag_parse(mypath="/report")
    diag = nil
    @xml_doc.elements.each("/report") do |report|
      diag = I_Diag.new(report)
      break
    end
    if diag == nil
      @xml_doc.elements.each("#{mypath}/error") do |element|
        @xml_doc.elements.each("#{mypath}") do |report|
          diag = I_Diag.new(report)
          break
        end
        break
      end
    end
    if diag == nil
      return false
    end
    add_user_accessor('PARM', diag)
	  return true
  end
  def out_xml
    s = @xml_doc.to_s
    s
  end
  def dump()
    msg = self.dump_error(true,true,true,true,true)
    msg
  end
  def dump_all()
    msg = self.dump_error(true,true,true,true,true)
    msg
  end
  def dump_error(xmlversion=true,xmlerror=true,jobinfo=true,jobscan=true,joblog=true)
    msg = "\n"
    # xmlservice error occurred?
    if self.xmlservice_error and defined? self.PARM
      msg << "=== Dump ===\n"
      # xmlservice version (all error modes)
      if xmlversion and defined? self.PARM.version
  	    msg << "Version\n"
        msg << "  version: #{self.PARM.version}\n"
      end
      # xmlservice job information (all error modes)
      if jobinfo and defined? self.PARM.jobinfo
        info = self.PARM.jobinfo
        msg << "Job\n"
        msg << " jobipc:      #{info.jobipc}\n"
        msg << " jobipcskey:  #{info.jobipcskey}\n"
        msg << " jobname:     #{info.jobname}\n"
        msg << " jobuser:     #{info.jobuser}\n"
        msg << " jobnbr:      #{info.jobnbr}\n"
        msg << " jobsts:      #{info.jobsts}\n"
        msg << " curuser:     #{info.curuser}\n"
        msg << " ccsid:       #{info.ccsid}\n"
        msg << " dftccsid:    #{info.dftccsid}\n"
        msg << " paseccsid:   #{info.paseccsid}\n"
        msg << " langid:      #{info.langid}\n"
        msg << " cntryid:     #{info.cntryid}\n"
        msg << " sbsname:     #{info.sbsname}\n"
        msg << " sbslib:      #{info.sbslib}\n"
        msg << " curlib:      #{info.curlib}\n"
        msg << " syslibl:     #{info.syslibl}\n"
        msg << " usrlibl:     #{info.usrlibl}\n"
      end
      # xmlservice fast internal error log (all error modes)
      if xmlerror and defined? self.PARM.error
  	    msg << "XMLError\n"
        for j in 0..self.PARM.error.count-1
          err = self.PARM.error[j]
	      msg << " #{err.errnoxml}"
	      msg << " : #{err.xmlerrmsg}"
	      msg << " : #{err.xmlhint}\n"
        end
      end
      # xmlservice joblog scan (error='on' or error='off')
      if jobscan and defined? self.PARM.joblogscan
        msg << "JoblogScan\n"
        for j in 0..self.PARM.joblogscan.joblogrec.count-1
          log = self.PARM.joblogscan.joblogrec[j]
	      msg << " #{log.jobcpf}"
	      msg << " : #{log.jobtime}\n"
	      msg << "         : #{log.jobtext}\n"
        end
      end
      # xmlservice joblog scan (error='on' or error='off')
      if joblog and defined? self.PARM.joblog
  	    msg << "Joblog\n"
        msg << "#{self.PARM.joblog.joblogdata}\n"
      end
    end
    msg
  end
  def reserved_words
    no = ["@xml_"]
    no
  end
  def format_inspect_include(name)
    self.reserved_words.each do |no|
      if name.include? no
        return false
      end
    end
    true
  end
  def dump_inspect
    out = ""
    out << "\n" << self.dump_inspect_input
    out << "\n" << self.dump_inspect_response
    out << "\n" << self.dump_inspect_returndata
    out
  end
  def dump_inspect_input
    out = "Input:\n" << self.format_inspect(@input,"input")
    out
  end
  def dump_inspect_response
    out = "Response:\n" << self.format_inspect(@response,"response")
    out
  end
  def dump_inspect_returndata
    out = "ReturnData:\n" << self.format_inspect(@returndata,"returndata")
    out
  end
  def format_inspect_recursive(lastvar,dot,alreadyseen,isInput)
# puts "top #{dot}#{lastvar.class.name}"
    out = ""
    if isInput
      if lastvar.kind_of? Hash
        outvar = Array.new
        lastvar.each do |k, v|
          out << dot + "[#{k}]\n"
          out << self.format_inspect_recursive(v,dot + ".",alreadyseen,isInput)
        end
      elsif lastvar.kind_of? Array
        outvar = Array.new
        for i in 0..lastvar.count-1
          out << dot + "[#{i}]\n"
          out << self.format_inspect_recursive(lastvar[i],dot + ".",alreadyseen,isInput)
        end
      else
        outvar = lastvar.instance_variables
      end
    else
      outvar = lastvar.instance_variables
    end
    outvar.each do |v|
      name = v.to_s
# puts "child #{dot}#{name}"
      if !self.format_inspect_include(name)
        next
      end
      go = true
      alreadyseen.each do |seen|
        if (dot + "#{name}").eql? "#{seen}"
          go = false
          break;
        end
      end
      if !go
        next
      end
      nextvar = lastvar.instance_variable_get(name)
# puts "next #{dot}#{name} #{nextvar.class.name}"
       if isInput
        if dot.eql? "." and !name.include? "PARM"
          next
        end
       else 
        if dot.eql? "."
          if (self.kind_of? I_PGM or self.kind_of? I_SRVPGM) and !(nextvar.kind_of? I_Return or nextvar.kind_of? I_Parameter)
            next
          end
        end
      end
      whatami = nextvar.class.name
      cln = ""
      if nextvar.instance_of? I_DS
        cln = "[i]"
      elsif nextvar.instance_of? Array
        cln = "[i]"
      elsif nextvar.instance_of? Hash
        cln = "[k,v]"
      end
      out << dot + "#{name}#{cln} -> #{whatami}\n"
      alreadyseen << dot + "#{name}"
      if !isInput and nextvar.kind_of? I_Base
        next
      end
      if !nextvar.nil?
        if nextvar.instance_of? Array
          i = 0
          nextvar.each do | value |
            out << dot + "[#{i}]\n"
            out << self.format_inspect_recursive(value,dot + ".",alreadyseen,isInput)
            i += 1
          end
        elsif nextvar.instance_of? Hash
          nextvar.each do | key, value |
            out << dot + "[#{key}]\n"
            out << self.format_inspect_recursive(value,dot + ".",alreadyseen,isInput)
          end
        else
          out << self.format_inspect_recursive(nextvar,dot + ".",alreadyseen,isInput)
        end
      end
    end
    out
  end
  def format_inspect(cat,top)
    if top.eql? "input"
      isInput = true
    else
      isInput = false
    end
    whatami = cat.class.name
    out = ""
    out2 = "@#{top} -> #{whatami}\n"
    out1 = ""
    if !cat.nil?
      alreadyseen = Array.new
      out2 << self.format_inspect_recursive(cat,".",alreadyseen,isInput)
      # shortcut methods
      shortcutmethods = cat.class.instance_methods
      alreadyseen.each do |name|
        shortcutmethods.each do |shortcut|
          if "#{name}".eql? "..@#{shortcut}"
            out1 << "obj." << top << ".#{shortcut}\n"
          end
        end
      end
    end
    out = out1 + out2
    out
  end

 end
 # ---------------------
 # xmlservice call PGM or SRVPGM
 # mypgm = XMLService::I_SRVPGM.new("ZZLOTS","ZZMANY","ZZLIB",{'error'=>'on'})
 # ===================================
 # paramters input/output (io='both')
 # ===================================
 # mypgm.input_parms do
 #  bin "mybin", 4               # mypgm.input.mybin = 'DEADBEEF'         PARM0
 #  char "mychar", 32            # mypgm.input.mychar = 'Hi there'        PARM1
 #  varchar "myvchar", 64        # mypgm.input.myvchar = 'Hi variable'    PARM2
 #  varchar4 "myvchar4", 1024    # mypgm.input.myvchar4 = 'Hi variable 4' PARM3
 #  tiny "mytiny"                # mypgm.input.mytiny = 1                 PARM4
 #  short "myshort"              # mypgm.input.myshort = 11               PARM5
 #  long "mylong"                # mypgm.input.mylong = 1111              PARM6
 #  longlong "myll"              # mypgm.input.myll = 11111111            PARM7
 #  utiny "myutiny"              # mypgm.input.myutiny = 1                PARM8
 #  ushort "myushort"            # mypgm.input.myushort = 11              PARM9
 #  ulong "myulong"              # mypgm.input.myulong = 1111             PARM10
 #  ulonglong "myull"            # mypgm.input.myull = 11111111           PARM11
 #  real "myreal4", 2            # mypgm.input.myreal4 = 11.11            PARM12
 #  float4 "myfloat4", 2         # mypgm.input.myfloat4 = 11.11           PARM13
 #  double "mydouble8", 4        # mypgm.input.mydouble8 = 1111.1111      PARM14
 #  float8 "myfloat8", 4         # mypgm.input.myfloat8 = 1111.1111       PARM15
 #  dec "mypack", 12, 2          # mypgm.input.mypack = 11111.11          PARM16
 #  zone "myzone", 8, 4          # mypgm.input.myzone = 11111.1111        PARM17
 #  tiny_enddo "mytinye"         # mypgm.input.mytinye = 1                PARM18  (enddo='mytinye')
 #  short_enddo "myshorte"       # mypgm.input.myshorte = 1               PARM19  (enddo='myshorte')
 #  long_enddo "mylonge"         # mypgm.input.mylonge = 1                PARM20  (enddo='mylonge')
 #  longlong_enddo "mylle"       # mypgm.input.mylle = 1                  PARM21  (enddo='mylle')
 #  utiny_enddo "myutinye"       # mypgm.input.myutinye = 1               PARM22  (enddo='myutinye')
 #  ushort_enddo "myushorte"     # mypgm.input.myushorte = 1              PARM23  (enddo='myutinye')
 #  ulong_enddo "myulonge"       # mypgm.input.myulonge = 1               PARM24  (enddo='myushorte')
 #  ulonglong_enddo "myulle"     # mypgm.input.myulle = 1                 PARM25  (enddo='myulle')
 #  real_enddo "myreal4e", 2     # mypgm.input.myreale = 1                PARM26  (enddo='myreale')
 #  float4_enddo "myfloat4e", 2  # mypgm.input.myfloat4e = 1              PARM27  (enddo='myfloat4e')
 #  double_enddo "mydouble8e", 4 # mypgm.input.double8e = 1               PARM28  (enddo='mydouble8e')
 #  float8_enddo "myfloat8e", 4  # mypgm.input.myfloat8e = 1              PARM29  (enddo='myfloat8e')
 #  dec_enddo "mypacke", 12, 2   # mypgm.input.mypacke = 1                PARM30  (enddo='mypacke') <-------------
 #  zone_enddo "myzonee", 8, 4   # mypgm.input.myzonee = 1                PARM31  (enddo='myzonee')              |
 #  struct "myds1",42 do         # mypgm.input.myds1                      PARM32                                 |
 #    varchar "ds1varchar", 64   # mypgm.input.myds1[0].ds1varchar = 'hi'                                        |
 #    dec "ds1packe", 12, 2      # mypgm.input.myds1[1].ds1packe = 222.22                                        |
 #    struct "myds2",42 do       # mypgm.input.myds1[2].myds2                                                    |
 #      char "ds2char", 32       # mypgm.input.myds1[2].myds2[0].ds2char = 'hi hi'                               |
 #      zone "ds2zone", 8, 4     # mypgm.input.myds1[2].myds2[1].ds2zone = 33.3333                               |
 #      struct_dou "myds3",42,"mypacke" do  # mypgm.input.myds1[2].myds2[2].myds3           (dou='mypacke') <-----
 #        char "ds3char", 32                # mypgm.input.myds1[2].myds2[2].myds3[0].ds3char = 'hi hi hi'        |
 #        zone "ds3zone", 8, 4              # mypgm.input.myds1[2].myds2[2].myds3[1].ds3zone = 44.4444           |
 #      end                                                                                                      |
 #    end                                                                                                        |
 #  end                                                                                                          |
 # end                                                                                                           |
 # ====================                                                                                          |
 # output only (return)                                                                                          |
 # ====================                                                                                          |
 # mypgm.return_parms do                                                                                         |
 #  struct "rtds1",42 do                    # mypgm.returndata.rtds1                                             |
 #    varchar "rt1vchar", 64                # mypgm.returndata.rtds1[0..42].rt1vchar                             |
 #    dec "rt1packed", 12, 2                # mypgm.returndata.rtds1[0..42].rt1packed                            |
 #    struct "rtds2",42 do                  # mypgm.returndata.rtds1[0..42].rtds2                                |
 #      char "rt2char", 32                  # mypgm.returndata.rtds1[0..42].rtds2[0..42].rt2char                 |
 #      zone "rt2zoned", 8, 4               # mypgm.returndata.rtds1[0..42].rtds2[0..42].rt2zoned                |
 #      struct_dou "rtds3",42,"mypacke" do  # mypgm.returndata.rtds1[0..42].rtds2[0..42].rtds3 (dou='mypacke') <--
 #        char "rt3char", 32                # mypgm.returndata.rtds1[0..42].rtds2[0..42].rtds3[0..mypacke].rt3char
 #        zone "rt3zoned", 8, 4             # mypgm.returndata.rtds1[0..42].rtds2[0..42].rtds3[0..mypacke].rt3zoned
 #      end
 #    end
 #  end
 # end
 # ==================== 
 # output 
 # ====================
 # puts mypgm.response.mybin
 # puts mypgm.response.mychar
 # puts mypgm.response.myvchar
 # puts mypgm.response.myvchar4 
 # puts mypgm.response.mytiny
 # puts mypgm.response.myshort
 # puts mypgm.response.mylong
 # puts mypgm.response.myll
 # puts mypgm.response.myutiny
 # puts mypgm.response.myushort
 # puts mypgm.response.myulong
 # puts mypgm.response.myull
 # puts mypgm.response.myreal4
 # puts mypgm.response.myfloat4
 # puts mypgm.response.mydouble8
 # puts mypgm.response.myfloat8
 # puts mypgm.response.mypack
 # puts mypgm.response.myzone
 # puts mypgm.response.mytinye
 # puts mypgm.response.myshorte
 # puts mypgm.response.mylonge
 # puts mypgm.response.mylle
 # puts mypgm.response.myutinye
 # puts mypgm.response.myushorte
 # puts mypgm.response.myulonge
 # puts mypgm.response.myulle
 # puts mypgm.response.myreale
 # puts mypgm.response.myfloat4e
 # puts mypgm.response.double8e
 # puts mypgm.response.myfloat8e
 # puts mypgm.response.mypacke
 # puts mypgm.response.myzonee
 # puts mypgm.response.myds1
 # puts mypgm.response.myds1[0..42].ds1varchar
 # puts mypgm.response.myds1[0..42].ds1packe
 # puts mypgm.response.myds1[0..42].myds2
 # puts mypgm.response.myds1[0..42].myds2[0..42].ds2char
 # puts mypgm.response.myds1[0..42].myds2[0..42].ds2zone
 # puts mypgm.response.myds1[0..42].myds2[0..42].myds3
 # puts mypgm.response.myds1[0..42].myds2[0..42].myds3[0..mypacke].ds3char
 # puts mypgm.response.myds1[0..42].myds2[0..42].myds3[0..mypacke].ds3zone
 # puts mypgm.returndata.rtds1
 # puts mypgm.returndata.rtds1[0..42].rt1vchar
 # puts mypgm.returndata.rtds1[0..42].rt1packed
 # puts mypgm.returndata.rtds1[0..42].rtds2
 # puts mypgm.returndata.rtds1[0..42].rtds2[0..42].rt2char
 # puts mypgm.returndata.rtds1[0..42].rtds2[0..42].rt2zoned
 # puts mypgm.returndata.rtds1[0..42].rtds2[0..42].rtds3 (dou='mypacke')
 # puts mypgm.returndata.rtds1[0..42].rtds2[0..42].rtds3[0..mypacke].rt3char
 # puts mypgm.returndata.rtds1[0..42].rtds2[0..42].rtds3[0..mypacke].rt3zoned
 # ---------------------
 class I_PGM < I_CALL
  def initialize(name,lib=nil,options=nil,func=nil)
    @xml_name = name
    @xml_lib = lib
    @xml_func = func
    @xml_parms = Array.new
    @xml_reti = nil
    @xml_doc = nil
    @xml_isParm = true
    super(options)
  end
  def name
    @xml_name
  end
  def lib
    @xml_lib
  end
  def func
    @xml_func
  end
  def getInputParameter(idx)
    parm = nil
    if @xml_parms.include?(idx)
      parm = @xml_parms[idx]
    end
    parm
  end
  def inputParameter(var,io,elem,idx=nil)
    if @xml_parms == nil
      @xml_parms = Array.new
    end
    if var == nil
      if !idx
        var = "PARM" << @xml_parms.count.to_s
      else
        var = "PARM" << idx.to_s
      end
    end
    if io == nil
      io = "io"
    end
    if !idx
      idx = @xml_parms.count
    end
    @xml_parms[idx] = I_Parameter.new(var,io,elem)
    @input.add_user_accessor(var,@xml_parms[idx].value)
    self.shortCut(@input,@xml_parms[idx])
  end
  def << (elem)
    self.inputParameter(nil,nil,elem)
    self
  end     
  def []= (idx,elem)
    self.inputParameter(nil,nil,elem,idx)
    self
  end
  def struct(var,dim,&block)
    ds = I_DS.new(var,dim).input_elems(&block)
    if @xml_isParm
      self.inputParameter(nil,nil,ds)
    else
      self.setReturn("xaggr",ds)
    end
    ds
  end  
  def bin(var, size, data="00")
    el = I_b.new(var,size,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xbin",el)
    end
    el
  end
  def char(var, size, data=" ")
    el = I_a.new(var,size,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xchr",el)
    end
    el
  end
  def varchar(var, size, data=" ")
    el = I_a_varying.new(var,size,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xvchr",el)
    end
    el
  end
  def varchar4(var, size, data=" ")
    el = I_a_varying_4.new(var,size,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xvchr4",el)
    end
    el
  end
  def tiny(var, data=0)
    el = I_Int8.new(var,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xtiny",el)
    end
    el
  end
  def short(var, data=0)
    el = I_Int16.new(var,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xshort",el)
    end
    el
  end
  def long(var, data=0)
    el = I_Int32.new(var,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xlong",el)
    end
    el
  end
  def longlong(var, data=0)
    el = I_Int64.new(var,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xll",el)
    end
    el
  end
  def utiny(var, data=0)
    el = I_Uint8.new(var,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xutiny",el)
    end
    el
  end
  def ushort(var, data=0)
    el = I_Uint16.new(var,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xushort",el)
    end
    el
  end
  def ulong(var, data=0)
    el = I_Uint32.new(var,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xulong",el)
    end
    el
  end
  def ulonglong(var, data=0)
    el = I_Uint64.new(var,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xull",el)
    end
    el
  end
  def real(var, scale, data=0.0)
    el = I_Float4.new(var,scale,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xreal",el)
    end
    el
  end
  def float4(var, scale, data=0.0)
    el = I_Float4.new(var,scale,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xfloat4",el)
    end
    el
  end
  def double(var, scale, data=0.0)
    el = I_Float8.new(var,scale,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xdouble",el)
    end
    el
  end
  def float8(var, scale, data=0.0)
    el = I_Float8.new(var,scale,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xfloat8",el)
    end
    el
  end
  def dec(var, size, scale, data=0)
    el = I_p.new(var,size,scale,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xdec",el)
    end
    el
  end
  def zone(var, size, scale, data=0)
    el = I_s.new(var,size,scale,data)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("xzone",el)
    end
    el
  end
  def struct_dou(var,dim,dou,&block)
    ds = I_DS.new(var,dim,dou).input_elems(&block)
    if @xml_isParm
      self.inputParameter(nil,nil,ds)
    else
      self.setReturn("douaggr",ds)
    end
    ds
  end  
  def tiny_enddo(var, data=0)
    el = I_Int8.new(var,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("doutiny",el)
    end
    el
  end
  def short_enddo(var, data=0)
    el = I_Int16.new(var,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("doushort",el)
    end
    el
  end
  def long_enddo(var, data=0)
    el = I_Int32.new(var,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("doulong",el)
    end
    el
  end
  def longlong_enddo(var, data=0)
    el = I_Int64.new(var,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("doull",el)
    end
    el
  end
  def utiny_enddo(var, data=0)
    el = I_Uint8.new(var,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("douutiny",el)
    end
    el
  end
  def ushort_enddo(var, data=0)
    el = I_Uint16.new(var,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("douushort",el)
    end
    el
  end
  def ulong_enddo(var, data=0)
    el = I_Uint32.new(var,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("douulong",el)
    end
    el
  end
  def ulonglong_enddo(var, data=0)
    el = I_Uint64.new(var,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("douull",el)
    end
    el
  end
  def real_enddo(var, scale, data=0.0)
    el = I_Float4.new(var,scale,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("doureal",el)
    end
    el
  end
  def float4_enddo(var, scale, data=0.0)
    el = I_Float4.new(var,scale,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("doufloat4",el)
    end
    el
  end
  def double_enddo(var, scale, data=0.0)
    el = I_Float8.new(var,scale,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("doudouble",el)
    end
    el
  end
  def float8_enddo(var, scale, data=0.0)
    el = I_Float8.new(var,scale,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("doufloat8",el)
    end
    el
  end
  def dec_enddo(var, size, scale, data=0)
    el = I_p.new(var,size,scale,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("doudec",el)
    end
    el
  end
  def zone_enddo(var, size, scale, data=0)
    el = I_s.new(var,size,scale,data,var)
    if @xml_isParm
      self.inputParameter(nil,nil,el)
    else
      self.setReturn("douzone",el)
    end
    el
  end
  def input_parms(&block)
     @xml_isParm = true
     self.instance_eval(&block)
     self
  end  
  def return_parms(&block)
     @xml_isParm = false
     self.instance_eval(&block)
     self
     @xml_isParm = true
  end  
  def parse_output_attr()
    super()
    @xml_doc.elements.each("/myscript/pgm/success") do |element|
      @response.add_user_accessor("output", element.text)
      break
    end
    @xml_doc.elements.each("/myscript/pgm/error") do |element|
      @response.add_user_accessor("output", element.text)
      break
    end
    idx = 0
    loop do
      parm = self.getIndexOutputParameter(idx)
      if parm
        @response.add_user_accessor(parm.var, parm)
        self.shortCut(@response,parm)
      else
        break
      end
      idx += 1
    end
  end
  def getIndexOutputParameter(idx)
    parm = nil
    i = 0
    @xml_doc.elements.each("/myscript/pgm/parm") do |element|
      if idx.instance_of? String
        if idx == element.attributes['var']
          parm = I_Parameter.new(element)
          break
        end
      else 
        if i == idx
          parm = I_Parameter.new(element)
          break
        end
      end
      i += 1
    end
    parm
  end
  def var(idx)
    var = "*NONE"
    parm = self.getIndexOutputParameter(idx)
    if parm
      var = parm.var
    end
    var
  end
  def value(idx)
    elem = nil
    parm = self.getIndexOutputParameter(idx)
    if parm
      elem = parm.value
    end
    elem
  end
  def outputParameter(idx)
    self.value(idx)
  end
  def outputParameter(idx)
    self.value(idx)
  end
  def assoc(idx)
    self.value(idx)
  end
  def [](idx)
    self.value(idx)
  end
  def setReturn(var,elem)
    @xml_reti = I_Return.new(var,elem)
    @input.add_user_accessor(var,@xml_reti.value)
  end
  def parse_return_attr()
    super()
    ret = self.ret()
    if ret
      @returndata.add_user_accessor(ret.var, ret)
      self.shortCut(@returndata,ret)
    end
  end
  def ret()
    ret = nil
    if @xml_doc
      @xml_doc.elements.each("/myscript/pgm/return") do |element|
        ret = I_Return.new(element)
      end
    end
    ret
  end
  def retValue()
    elem = nil
    ret = self.ret()
    if ret
      elem = ret.value
    end
    elem
  end
  def retVar()
    var = "*NONE"
    ret = self.ret()
    if ret
      var = ret.var
    end
    var
  end
  def parse_diag_attr()
    return self.xmlservice_diag_parse("/myscript/pgm")
  end
  def to_xml
    xml = ""
    lib = ""
    if @xml_lib
      lib = " lib='#{@xml_lib}'"
    end
    func = ""
    if @xml_func
      func = " func='#{@xml_func}'"
    end
    error = " error='fast'"
    opm = ""
    if @xml_options
      if @xml_options.has_key?(:error)
        error = " error='#{@xml_options[:error]}'"
      end
      if @xml_options.has_key?(:opm)
        opm = " opm='#{@xml_options[:opm]}'"
      end
    end
    xml = "<pgm name='#{@xml_name}'#{func}#{lib}#{error}#{opm}>\n"
    if @xml_parms
      @xml_parms.each do |a|
        xml << a.to_xml
      end
    end
    if @xml_reti
      xml << @xml_reti.to_xml
    end
    xml << "</pgm>\n"
    xml
  end
 end
 # ---------------------
 # xmlservice call SRVPGM
 # ---------------------
 class I_SRVPGM < I_PGM
  def initialize(name,func,lib=nil,options=nil)
    super(name,lib,options,func)
  end
 end
 # ---------------------
 # xmlservice call CMD
 # ---------------------
 class I_CMD < I_CALL
  def initialize(cmd,options=nil)
    @xml_cmd = cmd
    super(options)
    if cmd.include? '?'
      if options == nil
        @xml_options = Hash.new
      end
      @xml_options[:exec] = 'rexx'
    end
    @xml_cmd_desc = Array.new
  end
  def parse_output_attr()
    super()
    @xml_doc.elements.each("/myscript/cmd/success") do |element|
      @response.add_user_accessor("output", element.text)
      break
    end
    @xml_doc.elements.each("/myscript/cmd/error") do |element|
      @response.add_user_accessor("output", element.text)
      break
    end
  end
  def parse_return_attr()
    super()
    @xml_doc.elements.each("/myscript/cmd/row/data") do |element|
      @xml_cmd_desc[@xml_cmd_desc.count] = element.attributes['desc']
      @returndata.add_user_accessor(element.attributes['desc'],element.text)
    end
  end
  def parse_diag_attr()
    return self.xmlservice_diag_parse("/myscript/cmd")
  end
  def to_xml
    error = " error='fast'"
    exec = " exec='system'"
    if @xml_options
      if @xml_options.has_key?(:error)
        error = " error='#{@xml_options[:error]}'"
      end
      if @xml_options.has_key?(:exec)
        exec = " exec='#{@xml_options[:exec]}'"
      end
    end
    xml = "<cmd #{exec}#{error}>"
    xml << @xml_cmd.to_s
    xml << "</cmd>\n"
    xml
  end
 end
 # ---------------------
 # xmlservice call SH
 # ---------------------
 class I_SH < I_CALL
  def initialize(sh,options=nil)
    @xml_sh = sh
    super(options)
  end
  def parse_output_attr()
    super()
    row = Array.new
    isrow = false
    @xml_doc.elements.each("/myscript/sh/row") do |element|
      row[row.count] = element.text
      isrow = true
    end
    if isrow
      @response.add_user_accessor("output", row)
    else
      text = "*NONE"
      @xml_doc.elements.each("/myscript/sh") do |element|
        text = element.text
      end
      @response.add_user_accessor("output", text)
    end
  end
  def parse_diag_attr()
    return self.xmlservice_diag_parse("/myscript/sh")
  end
  def to_xml
    rows = ""
    error = " error='fast'"
    if @xml_options
      if @xml_options.has_key?(:error)
        error = " error='#{@xml_options[:error]}'"
      end
      if @xml_options.has_key?(:rows)
        error = " rows='#{@xml_options[:rows]}'"
      end
    end
    xml = "<sh #{rows}#{error}>"
    xml << @xml_sh.to_s
    xml << "</sh>\n"
    xml
  end
 end
 # ---------------------
 # xmlservice call DB2
 # ---------------------
 class I_DB2 < I_CALL
  def initialize(query,parms=nil,options=nil)
    @xml_query = query
    @xml_parms = parms
    super(options)
  end
  def parse_output_attr()
    super()
    row = Array.new
    isrow = false
    @xml_doc.elements.each("/myscript/sql/fetch/row") do |element|
      isrow = true
      col = Hash.new
      element.each do |child|
        col[child.attributes['desc']] = child.text
      end
      row[row.count] = col
    end
    if isrow
      @response.add_user_accessor("output", row)
    else
      text = "*NONE"
      @response.add_user_accessor("output", text)
    end
  end
  def parse_return_attr()
    super()
    @xml_doc.elements.each("/myscript/sql/execute/parm") do |element|
      @returndata.add_user_accessor(element.attributes['var'],element.text)
    end
  end
  def parse_diag_attr()
    rc = self.xmlservice_diag_parse("/myscript/sql/fetch")
    if rc
      rc = self.xmlservice_diag_parse("/myscript/sql/execute")
    end
    if rc
      rc = self.xmlservice_diag_parse("/myscript/sql/prepare")
    end
    rc
  end
  def to_xml
    rows = ""
    error = " error='fast'"
    if @xml_options
      if @xml_options.has_key?(:error)
        error = " error='#{@xml_options[:error]}'"
      end
    end
    xml = "<sql>\n"
    xml << "<prepare #{error}>"
    xml << @xml_query.to_s
    xml << "</prepare>\n"
    if @xml_parms
      xml << "<execute #{error}>\n"
      if @xml_parms.is_a?(XMLService::I_Base)
        parms = @xml_parms.value
      else
        parms = @xml_parms
      end
      if parms.is_a?(Array)
        i = 0
        parms.each do |v|
          n = " var='parm#{i.to_s}'"
          xml << "<parm io='both' #{n}>#{v}</parm>\n"
          i += 1
        end
      elsif parms.is_a?(Hash)
        parms.each do |a,v|
          n = " var='#{a}'"
          xml << "<parm io='both' #{n}>#{v}</parm>\n"
        end
      else
        n = " var='parm0'"
        v = parms
        xml << "<parm io='both' #{n}>#{v}</parm>\n"
      end
      xml << "</execute>\n"
    else
      xml << "<execute #{error}/>\n"
    end
    xml << "<fetch block='all' desc='on' #{error}/>\n"
    xml << "</sql>\n"
    xml
  end
 end
end # module XMLService


