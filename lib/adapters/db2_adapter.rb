require 'adapters/abstract_adapter'
require 'active_record'                                                          
require 'ibm_db'

module XMLServiceAdapters
  class IBM_DBAdapter < AbstractAdapter
    def initialize(conn,options)
      # yaml connection string "ActiveRecord",
      # so use if active, or start new connection 
      if conn.to_s == "ActiveRecord"
        for i in 0..1
          begin
            conn = ActiveRecord::Base.connection
          rescue
            ActiveRecord::Base.establish_connection(options)
          end
        end
      end
      # raw_connection gives access to actual IBM_DB::Connection
      if conn.is_a?(ActiveRecord::ConnectionAdapters::IBM_DBAdapter)
        super(conn.raw_connection, options)
      elsif conn.is_a?(IBM_DB::Connection)
        super(conn, options)
      else
        raise "#{self.class.name} invalid connection."
      end
    end
    def adapter_name
      'XMLSERVICE_IBM_DB'
    end
    def xmlservice(callme)
      case @xml_size 
      when @xml_size<=4096
        sql = "CALL #{@xml_install}.iPLUG4K(?,?,?,?)"
      when @xml_size>4096 && @xml_size<=32768
        sql = "CALL #{@xml_install}.iPLUG32K(?,?,?,?)"
      when @xml_size>32768 && @xml_size<=65536
        sql = "CALL #{@xml_install}.iPLUG65K(?,?,?,?)"
      when @xml_size>65536 && @xml_size<=524288
        sql = "CALL #{@xml_install}.iPLUG512K(?,?,?,?)"
      when @xml_size>524288 && @xml_size<=1048576
        sql = "CALL #{@xml_install}.iPLUG1M(?,?,?,?)"
      when @xml_size>1048576 && @xml_size<=5242880
        sql = "CALL #{@xml_install}.iPLUG5M(?,?,?,?)"
      when @xml_size>5242880 && @xml_size<=10485760
        sql = "CALL #{@xml_install}.iPLUG10M(?,?,?,?)"
      when @xml_size>10485760
        sql = "CALL #{@xml_install}.iPLUG15M(?,?,?,?)"
      else
        sql = "CALL #{@xml_install}.iPLUG512K(?,?,?,?)"
      end
      ctl = @xml_ctl
      ipc = @xml_ipc
      xmlin = ""
      xmlin << @xml_head
      xmlin << "\n<myscript>\n"
      xmlin << callme.to_xml
      xmlin << "</myscript>\n"
      xmlout = ""
      stmt = IBM_DB::prepare(@xml_conn, sql)
      ret = IBM_DB::bind_param(stmt, 1, "ipc", IBM_DB::SQL_PARAM_INPUT)
      ret = IBM_DB::bind_param(stmt, 2, "ctl", IBM_DB::SQL_PARAM_INPUT)
      ret = IBM_DB::bind_param(stmt, 3, "xmlin", IBM_DB::SQL_PARAM_INPUT)
      ret = IBM_DB::bind_param(stmt, 4, "xmlout", IBM_DB::SQL_PARAM_OUTPUT)
      ret = IBM_DB::execute(stmt)
      @xml_xmlout = xmlout
      @xml_doc = nil
    end
  end
end # module XMLServiceAdapters
