require 'adapters/abstract_adapter'
require 'net/http'
require 'uri'

module XMLServiceAdapters
  class RESTAdapter < AbstractAdapter
    def adapter_name
      'XMLSERVICE_REST'
    end
    def xmlservice(callme)
      xmlin = ""
      xmlin << @xml_head
      xmlin << "\n<myscript>\n"
      xmlin << callme.to_xml
      xmlin << "</myscript>\n"
      xmlout = ""
      post_args = { 
       :db2 => @xml_database,
       :uid => @xml_username,
       :pwd => @xml_password,
       :ipc => @xml_ipc,
       :ctl => @xml_ctl,
       :xmlin => URI::encode(xmlin),
       :xmlout => @xml_size
      }
      uri = URI(@xml_conn)
      res = Net::HTTP.post_form(uri, post_args)
      @xml_xmlout = res.body
      @xml_doc = nil
    end
  end 
end # module XMLServiceAdapters
