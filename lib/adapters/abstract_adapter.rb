require "rexml/document"

module XMLServiceAdapters
  # adapter factory (singleton)
  def self.adapter_factory(config)
    conn = config[:connection]
    # rest adapter (no db2 connections)
    if conn.to_s.include?"/"
      require 'adapters/rest_adapter'
      adapter = RESTAdapter.new(conn, config)
    # assume db2 adpater
    else
      require 'adapters/db2_adapter'
      adapter = IBM_DBAdapter.new(conn, config)
    end
    adapter
  end
  class AbstractAdapter
    def initialize(conn,options)
      @xml_conn = conn
      @xml_options = options
      if !options.has_key?(:install)
        options[:install] = ActiveXMLService::XINSTALL
      end
      if !options.has_key?(:ctl)
        options[:ctl] = ActiveXMLService::XCTL
      end
      if !options.has_key?(:ipc)
        options[:ipc] = ActiveXMLService::XIPC
      end
      if !options.has_key?(:size)
        options[:size] = ActiveXMLService::XSIZE
      end
      if !options.has_key?(:head)
        options[:head] = ActiveXMLService::XHEAD
      end
      if !options.has_key?(:database)
        options[:database] = ActiveXMLService::XDATABASE
      end
      if !options.has_key?(:username)
        options[:username] = ActiveXMLService::XUSERNAME
      end
      if !options.has_key?(:password)
        options[:password] = ActiveXMLService::XPASSWORD
      end
      @xml_install = options[:install]
      @xml_ctl = options[:ctl]
      @xml_ipc = options[:ipc]
      @xml_size = options[:size]
      @xml_head = options[:head]
      @xml_database = options[:database]
      @xml_username = options[:username]
      @xml_password = options[:password]
      @xml_xmlin = ""
      @xml_xmlin << @xml_head
      @xml_xmlout = ""
      @xml_xmlout << @xml_head
      @xml_doc = nil
    end
    def adapter_name
      'Abstract'
    end
    def xmlservice(callme)
    end
    def raw_connection
      @xml_conn
    end
    def raw_options
      @xml_options
    end
    def out_xml
      @xml_xmlout
    end
    def out_doc
      if @xml_doc == nil
        @xml_doc = REXML::Document.new(@xml_xmlout)
      end
      @xml_doc
    end
  end # class AbstractAdapter
end # module XMLServiceAdapters

