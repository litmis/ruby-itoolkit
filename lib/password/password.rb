require 'openssl'
require "base64"
require "uri"
require 'pathname'

# Sample utility for encrypted password
# require 'active_record'
# require 'ibm_db'
# $db = "LP0364D"
# $user = "DB2"
# $pass = "NICE2DB2"
# ActiveXMLService::Base.establish_connection(
#    :adapter    => 'ibm_db',
#    :database   => $db,
#    :username   => $user,
#    :password   => $pass
#    :connection => "ActiveRecord"
#    :install    => XMLSERVICE
#    :ctl        => "*sbmjob"
#    :ipc        => /tmp/ruby042
#    :size       => 15000000
#    :head       => "<?xml version='1.0'?>"
# )
# key = ActiveXMLService::Base.generate_key()
# puts "key is #{key}"
# penc = ActiveXMLService::Base.generate_password($pass,key)
# puts "password ( #{$pass} ) + ( key #{key} ) = encrypted password ( #{penc} )"

# Sample yaml files 
# 1) config/database.yml -- standard config/database.yml loaded by Rails automatic
# 2) /path/password.yml  -- operator maintained user/password/attributes file (QTMHHTTP access only) 
# 3) /path/key.yml       -- operator maintained passkey file used in runtime decode password (QTMHHTTP access only) 
# ==== file 1) standard database.yml (key="development" RAILS_ENV) =====
# xmlservice: &xmlservice
#   connection: "ActiveRecord"
#   install: XMLSERVICE
#   ctl: "*sbmjob"
#   ipc: /tmp/ruby042
#   size: 15000000
#   head: "<?xml version='1.0'?>"
# development:
#   adapter: ibm_db
#   database: LP0364D
#   username: DB2
#   pwd_yaml: /path/password.yml
#   <<: *xmlservice
# production:
#   adapter: ibm_db
#   database: LP0364D
#   username: DB2
#   pwd_yaml: /path/password.yml
#   <<: *xmlservice
# test:
#   adapter: ibm_db
#   database: LP0364D
#   username: DB2
#   pwd_yaml: /path/password.yml
#   <<: *xmlservice
# ==== file 2) password.yml (key="username")=====
# key_yaml: /path/key.yml
# DB2: 
#   pwd_enc: "YiYNfodSh5MGZVX7TXktEPSrnVlrAPjoyzzn48SdC/k=%0A"
#            ... possibly other attributes custom DB2, db2_yaml.yml, etc
# user2: 
#   pwd_enc: "YiYNfodSh5MGOHBOYFLINROCKIlrAPjoyzzn48SdC/k=%0A"
#            ... possibly other attributes custom DB2, db2_yaml.yml, etc
# ==== file 3) key.yml (key="*ALL") ===== (used runtime)
# pwd_key: "YIPS4321AAAAAAAAAAAAAAA132424245"
#          ... possibly other attributes, DB2, etc ...
module XMLSERVICEPassword
  PWD_TYPE = "aes-256-cbc"                          # encryption type
  PWD_KEY = "YIPS4321AAAAAAAAAAAAAAA132424245"      # 32 characters required
                                                    # of course PWD_KEY should not be in script
                                                    # but this is an example (people read)
  class Encrypt
    @xml_pwdkey = nil

    # ------------------
    # callable functions
    # ------------------

    # decrypt using password.yml (possible key.yml) (see top module)
    def self.parse_user_config(config)
      if config.has_key?(:username)
        if config.has_key?(:pwd_yaml)
          # password.yml file
          config = XMLSERVICEPassword::Encrypt.parse_yaml(config, config[:pwd_yaml].to_s, config[:username].to_s)
          config = XMLSERVICEPassword::Encrypt.symbolize_keys(config)
        end
        if config.has_key?(:pwd_enc)
          if !config.has_key?(:pwd_key)
            config[:pwd_key] = "*DEFAULT"
          end
          config[:password] = XMLSERVICEPassword::Encrypt.decrypt(config[:pwd_enc].to_s, config[:pwd_key].to_s)
        end
      end
      config
    end

    # password decrypt
    def self.decrypt(encoded, key = "*DEFAULT")
      XMLSERVICEPassword::Encrypt.establish_key(key);
      decoded = XMLSERVICEPassword::Encrypt.decode(encoded)
      cipher = OpenSSL::Cipher::Cipher.new(PWD_TYPE)
      cipher.decrypt
      cipher.key = @xml_pwdkey
      cipher.iv = decoded.slice!(0,16) # Remove the IV from the encrypted data
      decrypted = cipher.update(decoded) + cipher.final
      decrypted 
    end

    # generate a pass key 
    def self.gen_key()
      XMLSERVICEPassword::Encrypt.establish_key("*DEFAULT");
      cipher = OpenSSL::Cipher::Cipher.new(PWD_TYPE)
      cipher.encrypt
      cipher.key = @xml_pwdkey
      all = " "
      for i in 1..3
        cipher.iv = iv = cipher.random_iv
        raw = XMLSERVICEPassword::Encrypt.encode(iv)
        all << raw.to_s
      end
      encode = all.slice!(1..32)
      encode
    end

    # generate a encrypted password 
    def self.gen_password(plaintext, key = "*DEFAULT")
      XMLSERVICEPassword::Encrypt.establish_key(key);
      cipher = OpenSSL::Cipher::Cipher.new(PWD_TYPE)
      cipher.encrypt
      cipher.key = @xml_pwdkey
      cipher.iv = iv = cipher.random_iv
      encrypted = cipher.update(plaintext) + cipher.final
      encrypted = iv + encrypted # Send along the IV
      encoded = XMLSERVICEPassword::Encrypt.encode(encrypted)
      encoded
    end

    # ------------------
    # helper function (maybe should be private)
    # ------------------

    # assure valid iv and key
    def self.establish_key(key = "*DEFAULT")
      if key && key != '*DEFAULT'
        @xml_pwdkey = key
      else
        @xml_pwdkey = PWD_KEY
      end
    end

    # base64 encode for web/yaml usage
    def self.encode(item)
      encrypted = Base64.encode64(item)
      encoded = URI.escape(encrypted)
      encoded
    end

    # base64 decode from web/yaml
    def self.decode(item)
      encrypted = URI.unescape(item)
      decoded = Base64.decode64(encrypted)
      decoded
    end

    # Converts all +config+ keys to symbols
    def self.symbolize_keys(config)
      # config = config.symbolize_keys
      config.keys.each do |key|
        config[(key.to_sym rescue key) || key] = config.delete(key)
      end
      config
    end


    # recursive *_yaml: /path/thing.yml
    def self.nest_yaml(config,key,value)
      if key.include? "_yaml" or (value != nil and value.instance_of? String and value.to_s.include? ".yml")
        # recursive /path/password.yml
        if value.include? "password.yml"
          if !config.has_key?(:username)
            if config.has_key?("username")
              config[:username] = config["username"]
            end
          end
          if config.has_key?(:username)
            config = XMLSERVICEPassword::Encrypt.parse_yaml(config, value, config[:username])
          end
        # recursive *_yaml: /path/thing.yml
        else
          config = XMLSERVICEPassword::Encrypt.parse_yaml(config, value, "*ALL")
        end
      end
      config
    end

    # parse nested yaml files
    def self.parse_yaml(config, yaml_file, yaml_key)
      rfile = Pathname.new(yaml_file)
      if rfile
        f = open(rfile.to_s)
        doc = YAML::load_stream( f )
        doc.each do |key, value|
         key.each do |key0, value0|
          # recursive *_yaml
          config = XMLSERVICEPassword::Encrypt.nest_yaml(config,key0,value0)
          # take everything
          if yaml_key == "*ALL"
            config[key0] = value0
          # found target key="username"
          elsif yaml_key == key0
            value0.each do |key1, value1|
              # recursive *_yaml
              config = XMLSERVICEPassword::Encrypt.nest_yaml(config,key1,value1)
              # take everything
              config[key1] = value1
            end
          end
         end
        end
      end
      config
    end
  end # Encrypt
end # XMLSERVICEPassword


