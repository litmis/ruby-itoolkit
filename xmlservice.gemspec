Gem::Specification.new do |gem|
  gem.name    = 'xmlservice'
  gem.version = '1.3.3'

  gem.summary = "IBM i Access"
  gem.description = "Call IBM i PGM, SRVPGM, CMD, PASE"

  gem.authors  = ['Tony Cairns','Aaron Bartell']
  gem.email    = 'team@litmis.com'
  gem.homepage = 'https://bitbucket.org/litmis/xmlservice'
  gem.licenses = ['BSD']

  gem.add_development_dependency 'rake'

  gem.files = Dir['{lib,test}/**/*', 'README*', 'LICENSE*', 'version*']
end

