#Introduction
[![Gem Version](https://badge.fury.io/rb/xmlservice.svg)](http://badge.fury.io/rb/xmlservice)

The `xmlservice` Gem is a Ruby wrapper over the [XMLSERVICE](http://youngiprofessionals.com/wiki/index.php/XMLService/XMLService) open source project from IBM.  This Gem is now jointly maintained by IBM and the [KrengelTech Litmis team](litmis.com).  You can find the previous Gem history [here](http://youngiprofessionals.com/wiki/index.php/XMLSERVICE/Ruby), but note all future work will be done in this repo.


#Install
To get the latest version of the `xmlservice` RubyGem you can simply install it from [rubygems.org](https://rubygems.org/gems/xmlservice), as shown below.
```
gem install xmlservice
```
-- or --

**Gemfile:**
```
gem 'xmlservice'
```
#Build/Install
If you are adding/changing xmlservice then you can do the following to get it into your environment (i.e. IFS on your machine) and build/install from scratch.
```
$ git clone git@bitbucket.org:litmis/xmlservice.git
$ cd xmlservice
$ gem build xmlservice.gemspec
$ gem install ./xmlservice-1.3.2.gem --local --force --ignore-dependencies --no-rdoc
```
Note that `xmlservice-1.3.2.gem` is the version of the Gem as of this writing. Please modify based on what `gem build...` produces.

#Other
```
$ cd xmlservice
$ ./zzall.sh
```

Run tests (script)
=================
###Change values
```
$ cd xmlservice
$ edit zztestdb2.sh zztestrest.sh
export TEST_URL=http://174.79.32.155/cgi-bin/xmlcgi.pgm
export TEST_USER=DB2
export TEST_PWD=PWD
```

###Run
```
$ cd xmlservice
$ ./zztestdb2.sh    (test db2 driver)
$ ./zztestrest.sh   (test rest driver)
```


Run tests (manually)
=================
###Set up
```
$ export PATH=/PowerRuby/{version}/bin:$PATH
$ export LIBPATH=/PowerRuby/{version}/lib
$ export TEST_URL=http://174.79.32.155/cgi-bin/xmlcgi.pgm --or-- ibm_db
$ export TEST_DATABASE=*LOCAL
$ export TEST_USER=*NONE
$ export TEST_PWD=*NONE
```
###Optional
```
$ export TEST_CTL=*here --or-- *sbmjob
$ export TEST_IPC=*na   --or-- /tmp/privatextoolkitjobroute  (*sbmjob) 
$ export TEST_XMLSERVICE=XMLSERVICE
$ export TEST_LIB=RUBYTEST
```
###Main directory
```
$ cd xmlservice
$ rake
```
###Test directory 
```
$ cd xmlservice/test
$ rake test
```
###Individual 
```
$ xmlservice/test/test_60000_toolkit_driver
$ ruby test_60310_DriverCMD.rb
```
#Apache Instance
If you want to do HTTP requests for your XMLSERVICE calls then you need to configure an Apache instance.  Below describes how to do all the steps, though you could do modify an existing Apache instance.

First, create the member, as shown below.
```
ADDPFM FILE(QUSRSYS/QATMHINSTC) MBR(XMLSERVICE) TEXT('XMLSERVICE')
```
Paste the following into member `QUSRSYS/QATMHINSTC,XMLSERVICE`.  (i.e. `UPDDTA FILE(QUSRSYS/QATMHINSTC) MBR(XMLSERVICE)`)
```
-apache -d /www/xmlservice -f conf/httpd.conf -AutoStartN
```

Open a PASE shell (i.e. `CALL QP2TERM`) and run the following commands.
```
mkdir -p /www/xmlservice/conf
mkdir -p /www/xmlservice/logs
mkdir -p /www/xmlservice/htdocs
cat >> /www/xmlservice/conf/httpd.conf << EOF
Listen *:8000
DocumentRoot /www/xmlservice/htdocs
ScriptAlias /cgi-bin/ /QSYS.LIB/XMLSERVICE.LIB/
<Directory /QSYS.LIB/XMLSERVICE.LIB/>
  order allow,deny
  allow from all
  SetHandler cgi-script
  Options +ExecCGI
</Directory>
EOF
```
Below are the commands to start and stop Apache instance that was just created.
```
STRTCPSVR SERVER(*HTTP) INSTANCE(XMLSERVICE)

ENDTCPSVR SERVER(*HTTP) INSTANCE(XMLSERVICE)
```

#Issues
If you run into issues then please [here](https://bitbucket.org/litmis/xmlservice/issues?status=new&status=open).

#License
MIT.   See [`LICENSE`](https://bitbucket.org/litmis/ruby-itoolkit/src) file.