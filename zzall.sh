#!/bin/bash
rm xmlservice-1.3.0.gem
rm -R xmlservice-1.3.0.gem.build
echo "build"
gem build xmlservice-1.3.0.gemspec
# echo "compile"
# gem compile ./xmlservice-1.3.0.gem
echo "install"
gem install ./xmlservice-1.3.0.gem --local --force --ignore-dependencies --no-rdoc
