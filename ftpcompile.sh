# bash ftpcompile.sh lp0364d adc
rm xmlservice-1.0.1.tar
tar -cf xmlservice-1.0.1.tar ./*

ftp -i -n -v $1 << ftp_end
user $2

quote namefmt 1

bin
cd /home/ADC/xmlservice-1.0.1.devel
put xmlservice-1.0.1.tar

quit

ftp_end

